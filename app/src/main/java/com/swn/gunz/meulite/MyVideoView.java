package com.swn.gunz.meulite;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.widget.VideoView;

public class MyVideoView extends VideoView {

	private Point requestedSize;
	
	public Point getRequestedSize() {
		return requestedSize;
	}

	public void setRequestedSize(Point requestedSize) {
		this.requestedSize = requestedSize;
	}

	public MyVideoView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	public MyVideoView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public MyVideoView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		//super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		setMeasuredDimension(requestedSize.x,requestedSize.y);
	}
	
	

}
