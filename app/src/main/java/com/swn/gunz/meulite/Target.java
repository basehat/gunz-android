package com.swn.gunz.meulite;

public class Target {
	
	public static final int DEFAULT_TARGET = R.drawable.target4;
	public static final int MAX_BULLETHOLES = 24;
	
	public static final int OFFSET_LEVEL0 = 40; //dip
	
	protected static final int TYPE_BROWSE = 1;
	protected static final int TYPE_CUSTOM = 2;
	
	protected int TYPE_CURRENT = TYPE_BROWSE;
	protected TargetSource TARGET_SOURCE;
	
	protected static final boolean DBG_MODE = false;
	public static final int FD_MAX_FACES = 2;
	
}
