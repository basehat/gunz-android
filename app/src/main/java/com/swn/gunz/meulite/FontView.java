package com.swn.gunz.meulite;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class FontView extends TextView {

	protected static final String FONT_ARMALITE = "fonts/armalite.ttf";
	protected static final String FONT_TOKYOSOFT = "fonts/tokyosoft.ttf";
	
	public FontView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public FontView(Context context) {
		super(context);
	}

	public FontView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	protected void setFont(String fontPath){
		try{
			Typeface tempFont = Typeface.createFromAsset(getContext().getAssets(), fontPath);
			setTypeface(tempFont);
			tempFont = null;
		} catch(Exception e){
		}
	}
	
}
