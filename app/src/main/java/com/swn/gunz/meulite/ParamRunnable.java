package com.swn.gunz.meulite;

class ParamRunnable implements Runnable
{
	private Object param;
	
	public Object getParam() {
		return param;
	}

	public void setParam(Object label) {
		this.param = label;
	}

	public ParamRunnable(Object p)
	{
		this.param = p;
	}

	public void run() {
	}
}