package com.swn.gunz.meulite;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.util.EncodingUtils;
import org.w3c.dom.Document;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class FireArms
{
	public final static String STATIC_GUNLABEL = "meu";
	
	public final static String GUNZ_GUN_DIRECTORY_GLOBAL = "/GunZ/Guns";
	public final static String GUNZ_GUN_ANIMATION_EXTENSION = ".png";
	public final static String GUNZ_GUN_ANIMATION_PREFIX    = "z_anim_";
	
	public final static String GUNZ_ZPACKAGE_PATH_RELOAD = "reload/";
	public final static String GUNZ_ZPACKAGE_PATH_SHOOT  = "shoot/";
	public final static String GUNZ_ZPACKAGE_PATH_SOUNDS = "sounds/";
	
	public final static String GUNZ_GUN_FILENAME_ZPACKAGE = "gun.zip";
	public final static String GUNZ_ZPACKAGE_FILENAME_CONFIG = "config.xml";
	public final static String GUNZ_ZPACKAGE_FILENAME_AXML = "animation.xml";
	public final static String GUNZ_ZPACKAGE_FILENAME_SOUND_SHOOT  = "shoot1.mp3";
	public final static String GUNZ_ZPACKAGE_FILENAME_SOUND_RELOAD = "reload.mp3";
	public final static String GUNZ_ZPACKAGE_FILENAME_SOUND_SLIDEBACK = "slideback.wav";
	
	public final static String GUNZ_ZPACKAGE_ROOT = "gun/";
	
	public static final String DESERT_EAGLE = "deagle";
	public static final String BERETTA_M9 = "m9";
	public static final String MEU_1911 = "meu";
	
	protected static final int WEAPONID_MEU = 1;
	protected static final int WEAPONID_USP45 = 2;
	protected static final int WEAPONID_T850 = 3;
	protected static final int WEAPONID_DEAGLE = 4;
	
	/**
	 * gets the full path to save a particular guns universal package.
	 * @return
	 */
	public static String getUniveralPackagePath(String gunid)
	{
		String coinshotDir = Environment.getExternalStorageDirectory().getAbsolutePath();
		coinshotDir += GUNZ_GUN_DIRECTORY_GLOBAL+"/"+gunid+"/";
		return coinshotDir;
	}
	
	public static boolean extractPackage(String gundir)
	{
		return(extractPackage(gundir,false));
	}
	
	public static boolean extractPackage(String gundir, boolean refresh)
	{
		try {
			
			String cacheDir = gundir+Util.GUNZ_GUN_DIRECTORY_CACHE;
			File fcacheDir = new File(cacheDir);
			
			if(!fcacheDir.isDirectory()){
				fcacheDir.mkdirs();
			}
			
			if((new File(cacheDir+GUNZ_ZPACKAGE_FILENAME_CONFIG).isFile())){
				if(refresh){
					//clean out existing cache
					Util.deleteDir(fcacheDir);
					fcacheDir.mkdirs();
				} else {
					//package already seems to still be extracted from last go
					return true;
				}
			}
			
			ZipFile z = new ZipFile(gundir+GUNZ_GUN_FILENAME_ZPACKAGE);
			
			Enumeration<? extends ZipEntry> entries = z.entries();
			
			while(entries.hasMoreElements()){
				
				ZipEntry entry = (ZipEntry)entries.nextElement();

				String dest = cacheDir+entry.getName().replace(GUNZ_ZPACKAGE_ROOT, "");
				
				if(entry.isDirectory()){
					if(AndroidGunz.DBG_MODE)
						AndroidGunz.log("unpacking directory: "+entry.getName());
					if(!entry.getName().equals(GUNZ_ZPACKAGE_ROOT))
						(new File(dest)).mkdir();
					continue;
				}
				
				if(AndroidGunz.DBG_MODE)
					AndroidGunz.log("unpacking file: "+entry.getName());
				
				Util.copyInputStream(z.getInputStream(entry),
						new BufferedOutputStream(new FileOutputStream(dest)));
			}
			
			z.close();
			
			return true;
			
		} catch (Exception e) {
			if(AndroidGunz.DBG_MODE)
				e.printStackTrace();
		}
		return false;
	}
	
	public static FirearmConfig parseConfig(String gundir){
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		
		FirearmConfig retObj = new FirearmConfig();
		
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e1) {
			if(AndroidGunz.DBG_MODE)
				e1.printStackTrace();
			builder = null;
		}
		
		if(builder==null)
			return retObj;
		
		try {
			
			String cacheDir = gundir+Util.GUNZ_GUN_DIRECTORY_CACHE;
			File fcacheDir = new File(cacheDir);
			
			if(!fcacheDir.isDirectory()){
				fcacheDir.mkdirs();
			}
			
			File gunConfig = new File(cacheDir+GUNZ_ZPACKAGE_FILENAME_CONFIG);
			
			if(!gunConfig.isFile())
				return retObj;
			
			//int resId = AndroidGunz.getCtx().getResources().getIdentifier(ident, "raw", AndroidGunz.getCtx().getPackageName());
			Document document = builder.parse(gunConfig);
			
			String magCapacity = document.getElementsByTagName(Util.CONFIG_TAGNAME_MAGAZINECAPACITY).item(0).getFirstChild().getNodeValue();
			String dischargeAnimations = document.getElementsByTagName(Util.CONFIG_TAGNAME_DISCHARGEANIMS).item(0).getFirstChild().getNodeValue();
			String mFlashOffsetX = document.getElementsByTagName(Util.CONFIG_TAGNAME_MFLASHOFFSETX).item(0).getFirstChild().getNodeValue();
			String mFlashOffsetY = document.getElementsByTagName(Util.CONFIG_TAGNAME_MFLASHOFFSETY).item(0).getFirstChild().getNodeValue();
			String mFlashEnabled = document.getElementsByTagName(Util.CONFIG_TAGNAME_MFLASHENABLED).item(0).getFirstChild().getNodeValue();
			
			if(AndroidGunz.DBG_MODE){
				Log.i("Firearm","Loading config");
				Log.i("Firearm","Found magazine capacity: "+magCapacity);
				Log.i("Firearm","Found discharge anims: "+dischargeAnimations);
				Log.i("Firearm","Found muzzle flash offset x: "+mFlashOffsetX);
				Log.i("Firearm","Found muzzle flash offset y: "+mFlashOffsetY);
			}
			
			retObj.magazineCap 		  = Integer.parseInt(magCapacity);
			retObj.dischareAnimations = Integer.parseInt(dischargeAnimations);
			retObj.muzzleFlashOffsetX = Integer.parseInt(mFlashOffsetX);
			retObj.muzzleFlashOffsetY = Integer.parseInt(mFlashOffsetY);
			retObj.hasMuzzleFlash 	  = mFlashEnabled.equals("false")?false:true;
			
		} catch (Exception e) {
			if(AndroidGunz.DBG_MODE)
				e.printStackTrace();
			factory = null;
		}
		factory = null;
		
		return retObj;
	}
	
	/**
	 * saves a gunZ universal animation package with optional encryption
	 * @param encrypt
	 */
	public static void saveGun(Context ctx, boolean encrypt)
	{
		
		String state = Environment.getExternalStorageState();
    	
    	if(!Environment.MEDIA_MOUNTED.equals(state) &&
    	   !Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))
    	{
    		Toast.makeText(ctx, R.string.t_message_no_storage, Toast.LENGTH_LONG).show();
    		return;
    	}
		
		try {
			
			String gunzPath = getUniveralPackagePath(ctx.getResources().getString(R.string.gun_id));
			String saveFile = gunzPath+GUNZ_GUN_FILENAME_ZPACKAGE;
			
			//make sure path exists
			new File(gunzPath).mkdirs();
			
			BufferedOutputStream fos =
					new BufferedOutputStream(new FileOutputStream(saveFile));
			
			ZipOutputStream zos = new ZipOutputStream(fos);
			
			//FileInputStream fin = new FileInputStream("/sdcard/GunZ/Guns/1/config.xml");
			
			zos.setLevel(Deflater.NO_COMPRESSION);
			
			zos.putNextEntry(new ZipEntry(GUNZ_ZPACKAGE_ROOT));
			
			//configs
			zos.putNextEntry(new ZipEntry(GUNZ_ZPACKAGE_ROOT+GUNZ_ZPACKAGE_FILENAME_CONFIG));
			int resId = ctx.getResources().getIdentifier(Util.CONFIG_PREFIX+STATIC_GUNLABEL, "raw", ctx.getPackageName());
			InputStream fis = ctx.getResources().openRawResource(resId);
			for (int c = fis.read(); c != -1; c = fis.read()) {
				zos.write(c);
			}
			fis.close();
			zos.closeEntry();
			
			//sounds
			zos.putNextEntry(new ZipEntry(GUNZ_ZPACKAGE_ROOT+GUNZ_ZPACKAGE_PATH_SOUNDS));
			zos.putNextEntry(new ZipEntry(GUNZ_ZPACKAGE_ROOT+GUNZ_ZPACKAGE_PATH_SOUNDS+GUNZ_ZPACKAGE_FILENAME_SOUND_SHOOT));
			resId = ctx.getResources().getIdentifier(STATIC_GUNLABEL+"_"+Util.ANIM_STAGE_SHOOT+"1",
													"raw",ctx.getPackageName());
			fis = ctx.getResources().openRawResource(resId);
			for (int c = fis.read(); c != -1; c = fis.read()) {
				zos.write(c);
			}
			fis.close();
			zos.closeEntry();
			
			zos.putNextEntry(new ZipEntry(GUNZ_ZPACKAGE_ROOT+GUNZ_ZPACKAGE_PATH_SOUNDS+GUNZ_ZPACKAGE_FILENAME_SOUND_RELOAD));
			resId = ctx.getResources().getIdentifier(STATIC_GUNLABEL+"_"+Util.ANIM_STAGE_RELOAD,
					"raw",ctx.getPackageName());
			fis = ctx.getResources().openRawResource(resId);
			for (int c = fis.read(); c != -1; c = fis.read()) {
				zos.write(c);
			}
			fis.close();
			zos.closeEntry();
			
			zos.putNextEntry(new ZipEntry(GUNZ_ZPACKAGE_ROOT+GUNZ_ZPACKAGE_PATH_SOUNDS+GUNZ_ZPACKAGE_FILENAME_SOUND_SLIDEBACK));
			resId = ctx.getResources().getIdentifier(STATIC_GUNLABEL+"_"+Util.ANIM_SOUND_SLIDEBACK,
					"raw",ctx.getPackageName());
			fis = ctx.getResources().openRawResource(resId);
			for (int c = fis.read(); c != -1; c = fis.read()) {
				zos.write(c);
			}
			fis.close();
			zos.closeEntry();
			
			
			//shoot animation
			zos.putNextEntry(new ZipEntry(GUNZ_ZPACKAGE_ROOT+GUNZ_ZPACKAGE_PATH_SHOOT));
			String ident = Util.DRAWABLE_PREFIX+STATIC_GUNLABEL+"_"+Util.ANIM_STAGE_SHOOT+"1";
			int xmlId = ctx.getResources().getIdentifier(ident, "xml", ctx.getPackageName());
			//Util.getAnimationFrames(AndroidGunz.getCtx(), xmlId)
			String[] frameNames = Util.parseXmlAnimation(ctx, xmlId);
			String xmlFile = "<resources>\n";
			int fCount = 1;
			for (String name : frameNames) {
				zos.putNextEntry(new ZipEntry(GUNZ_ZPACKAGE_ROOT+GUNZ_ZPACKAGE_PATH_SHOOT+GUNZ_GUN_ANIMATION_PREFIX+fCount+GUNZ_GUN_ANIMATION_EXTENSION));
				
				int tmpId = ctx.getResources().getIdentifier(name,
												"drawable", ctx.getPackageName());
				BufferedInputStream bis = new BufferedInputStream(
													ctx.getResources().openRawResourceFd(tmpId).createInputStream(),
													8192*10);
				for (int c = bis.read(); c != -1; c = bis.read()) {
					zos.write(c);
				}
				
				zos.closeEntry();
				bis.close();
				
				xmlFile += "\t<frame drawable=\""+GUNZ_GUN_ANIMATION_PREFIX+fCount+"\" />\n";
				
				fCount++;
			}
			
			xmlFile += "</resources>";
			
			zos.putNextEntry(new ZipEntry(GUNZ_ZPACKAGE_ROOT+GUNZ_ZPACKAGE_PATH_SHOOT+GUNZ_ZPACKAGE_FILENAME_AXML));
			zos.write(EncodingUtils.getAsciiBytes(xmlFile));
			zos.closeEntry();
			xmlFile = null;
			
			//reload animation
			zos.putNextEntry(new ZipEntry(GUNZ_ZPACKAGE_ROOT+GUNZ_ZPACKAGE_PATH_RELOAD));
			ident = Util.DRAWABLE_PREFIX+STATIC_GUNLABEL+"_"+Util.ANIM_STAGE_RELOAD;
			xmlId = ctx.getResources().getIdentifier(ident, "xml", ctx.getPackageName());
			//Util.getAnimationFrames(AndroidGunz.getCtx(), xmlId)
			frameNames = Util.parseXmlAnimation(ctx, xmlId);
			xmlFile = "<resources>\n";
			fCount = 1;
			for (String name : frameNames) {
				zos.putNextEntry(new ZipEntry(GUNZ_ZPACKAGE_ROOT+GUNZ_ZPACKAGE_PATH_RELOAD+GUNZ_GUN_ANIMATION_PREFIX+fCount+GUNZ_GUN_ANIMATION_EXTENSION));
				
				int tmpId = ctx.getResources().getIdentifier(name,
												"drawable", ctx.getPackageName());
				BufferedInputStream bis = new BufferedInputStream(
													ctx.getResources().openRawResourceFd(tmpId).createInputStream(),
													8192*10);
				for (int c = bis.read(); c != -1; c = bis.read()) {
					zos.write(c);
				}
				
				zos.closeEntry();
				bis.close();
				
				xmlFile += "\t<frame drawable=\""+GUNZ_GUN_ANIMATION_PREFIX+fCount+"\" />\n";
				
				fCount++;
			}
			
			xmlFile += "</resources>";
			
			zos.putNextEntry(new ZipEntry(GUNZ_ZPACKAGE_ROOT+GUNZ_ZPACKAGE_PATH_RELOAD+GUNZ_ZPACKAGE_FILENAME_AXML));
			zos.write(EncodingUtils.getAsciiBytes(xmlFile));
			zos.closeEntry();
			xmlFile = null;
			
			zos.close();
			fos.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
