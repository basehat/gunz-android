
package com.swn.gunz.meulite;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MenuActivity extends Activity {

	private static final boolean SHOW_VIDEO = false;
	
	private static Options o;
	private ImageView vCon;
	
	private static ProgressDialog dialog;
	
	private Menus menuController;
	
	public static final int INSTALL_SUCCESS = 0;
	public static final int INSTALL_ERROR_EXISTS = 1;
	public static final int INSTALL_ERROR_SDCARD = 2;
	
	private Handler mHandler = new Handler();
	
	protected ProgressDialog pd;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setTheme(android.R.style.Theme_Light_NoTitleBar);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		
		menuController = new Menus(this);
		
		getWindow().makeActive();
        getWindow().setFormat(PixelFormat.TRANSPARENT);
        
        setContentView(R.layout.mainmenu);
        o = new Options(getApplicationContext());
        
        Button iv = (Button)findViewById(R.id.b_play);
        Button iv2 = (Button)findViewById(R.id.b_info);
        Button iv3 = (Button)findViewById(R.id.b_install);

        iv3.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(!AndroidGunz.LITE_MODE){
					pd = ProgressDialog.show(MenuActivity.this, "", 
	                        getString(R.string.t_msg_installing), true);
					
					new Thread(new Runnable() {
						public void run() {
							Integer res = Util.installGunPack(MenuActivity.this);
							mHandler.post(new ParamRunnable(res) {
								public void run() {
									switch((Integer)getParam())
									{
										case(INSTALL_SUCCESS):
											Toast.makeText(MenuActivity.this, R.string.t_msg_install_ok, Toast.LENGTH_LONG).show();
											break;
										case(INSTALL_ERROR_EXISTS):
											Toast.makeText(MenuActivity.this, R.string.t_msg_install_error, Toast.LENGTH_LONG).show();
											break;
										case(INSTALL_ERROR_SDCARD):
											Toast.makeText(MenuActivity.this, R.string.t_msg_install_error2, Toast.LENGTH_LONG).show();
											break;
									}
									pd.dismiss();
								}
							});
						}
					}).start();
				}
			}
		});
        
        
        vCon = (ImageView)findViewById(R.id.vid_intro);
//        vCon.bringToFront();
        
        /*vCon.post(new Runnable() {
			public void run() {
				dialog = ProgressDialog.show(MenuActivity.this,
						"First Run!", 
                        "Installing Gun Package...", true);
				new Thread(new Runnable() {
					public void run() {
						FireArms.saveGun(MenuActivity.this, false);
						dialog.dismiss();
					}
				}).start();
			}
		});*/
        
        
        if(SHOW_VIDEO)
        {
        	//DisplayMetrics dm = getResources().getDisplayMetrics();
            //vCon.setRequestedSize(new Point(dm.widthPixels,dm.heightPixels));
        	
	       // Uri uri;
        
	        ///uri = Uri.parse("android.resource://"+getPackageName()+"/"+ R.raw.output_hdpi2);
        
	        /*vCon.setVideoURI(uri);
        
	        vCon.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
				
				public void onCompletion(MediaPlayer mp) {
					//mp.reset();
					vCon.setBackgroundResource(R.drawable.mainmenu);
					//mp.reset();
					//vCon.setVisibility(View.GONE);
					//iv3.bringToFront();
					iv.bringToFront();
					iv2.bringToFront();
				}
			});
	        vCon.setOnErrorListener(new MediaPlayer.OnErrorListener() {
				
				public boolean onError(MediaPlayer mp, int what, int extra) {
					//mp.release();
					return false;
				}
			});
	        
	        vCon.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
				
				public void onPrepared(MediaPlayer mp) {
					vCon.start();
				}
			});*/
        }
        else
        {
        	vCon.setBackgroundResource(R.drawable.mainmenu);
			iv.bringToFront();
			iv2.bringToFront();
        }
        
        iv.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				
				MediaPlayer mp = MediaPlayer.create(MenuActivity.this,R.raw.meu_slideback);
				if(mp!=null)
				{
					try{
					mp.setOnCompletionListener(mpComplete);
					mp.start();
					} catch(IllegalStateException e){}
				}
				
				//Intent i = new Intent(MenuActivity.this,AndroidGunz.class);
				//startActivity(i);
				
				showDialog(Menus.MENU_MODE);
				
			}
		});
        iv2.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				
				MediaPlayer mp = MediaPlayer.create(MenuActivity.this,R.raw.meu_slideback);
				mp.setOnCompletionListener(mpComplete);
				mp.start();
				
				//showDialog(Menus.MENU_OOM);
				
				Intent i = new Intent(MenuActivity.this,BioActivity.class);
				startActivity(i);
			}
		});
        
       /* iv3.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				dialog = ProgressDialog.show(MenuActivity.this,
						"First Run!", 
                        "Installing Gun Package...", true);
				new Thread(new Runnable() {
					public void run() {
						FireArms.saveGun(MenuActivity.this, false);
						dialog.dismiss();
					}
				}).start();
			}
		});*/
        
	}
	
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if(keyCode == KeyEvent.KEYCODE_BACK){
			showDialog(Menus.MENU_EXIT);
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	
	
	protected void onResume() {
		
		super.onResume();
		
		if(BioActivity.OOM_ERROR){
			BioActivity.OOM_ERROR = false;
			showDialog(Menus.MENU_OOM);
		}
	}
	
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		int test = resultCode;
		
		if(test == AndroidGunz.ACTIVITY_RESULT_OOM) {
        	showDialog(Menus.MENU_OOM);
        }
		
	}

	protected static OnCompletionListener mpComplete = new OnCompletionListener() {
		
		public void onCompletion(MediaPlayer mp) {
			mp.release();
		}
	};
	
	
	protected Dialog onCreateDialog(int id) {
		return menuController.prepareDialog(id);
	}
	
	public static Options getOptions(Context c) {
		if(o==null)
			o = new Options(c);
		return o;
	}

}
