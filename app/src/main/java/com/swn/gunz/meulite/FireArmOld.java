package com.swn.gunz.meulite;

import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.media.MediaPlayer;
import android.util.Log;

class FireArmOld {
	protected static final boolean DBG_MODE = false;
	
	protected static final int DOUBLETAP_DELAY = 50;//ms
	
	/**
	 * All guns must be listed here.
	 */
	protected static final String WEAPON_DESERT_EAGLE = "deagle";
	protected static final String WEAPON_BERETTA_M9 = "m9";
	protected static final String WEAPON_MEU_1911 = "meu";
	
	/**
	 *	Global Firearm configuration defaults.
	 */
	protected String gunName = FireArmOld.WEAPON_MEU_1911;
	protected int	 gunDischargeAnimsCount = 1;
	private static	  long	 gunVibrateTime = 100;//ms
	private boolean enableHeadshots = false;
	private boolean enableFaceDetection = false;
	private boolean enableHitboxes = false;
	
	/**	 **/

	protected FirearmConfig config = null;
	
	private MediaPlayer[] mpPool;
	private static String currentAnimationName = null;
	private String currentStage = null;
	//private int currentLevel = FireArmOld.ANIM_LEVEL_FIRST;
	public int roundsLeft = 0;
	private boolean ready = false;
	private boolean isFiring = false;
	public static Bitmap[] fireAnimation;
	public Bitmap[] fireLastAnimation;
	public Bitmap[] reloadAnimation;
	//private ImageView mFlash;
	//private double nextRandom = 0.0;
	private int shotCount = 0;
	private int shotTimer = 0;
	//private AnimationDrawable mFlashAnimation;
	//private AnimationDrawable mFlashAnimation2;
	//private Animation mFlashModifier;
	private static Timer t;
	public GameController fv;
	private static Runnable soundPlaylist = null;
	
	public int getShotCount() {
		return shotCount;
	}

	public void setShotCount(int shotCount) {
		this.shotCount = shotCount;
	}
/*
	public void destroy(){
		
		t.cancel();
		t = null;
		
		fv.destroy();
		fv = null;
		
		soundPlaylist = null;
		
		if(fireAnimation!=null){
			for (Bitmap b : fireAnimation) {
				b.recycle();
				b = null;
			}
		}
		fireAnimation = null;
		
		if(fireLastAnimation!=null){
			for (Bitmap b : fireLastAnimation) {
				b.recycle();
				b = null;
			}
			fireLastAnimation = null;
		}
		
		if(reloadAnimation!=null){
			for (Bitmap b : reloadAnimation) {
				b.recycle();
				b = null;
			}
			reloadAnimation = null;
		}
	}
	
	public FireArmOld(String weapon){
		mpPool = new MediaPlayer[5];
		gunName = weapon;
		config = new FirearmConfig();
		parseConfig();
		roundsLeft = config.magazineCap;
		fv = AndroidGunz.gameView;
		
		if(!config.johnWoo)
			AndroidGunz.setRoundCount(roundsLeft);
		
		t = new Timer();
	}
	
	protected void preload(){
		
			if(fireAnimation==null)
				fireAnimation =  getAnimation(ANIM_STAGE_SHOOT,1);
			//fireAnimation =  getAnimation(ANIM_STAGE_RELOAD);
			if(reloadAnimation==null)
				reloadAnimation = getAnimation(ANIM_STAGE_RELOAD);
			//reloadAnimation = fireLastAnimation;
			
			fv.setIdleAnimation(fireAnimation[fireAnimation.length-1]);
			
			if(config.hasMuzzleFlash){
				if(fv.gunThread.flashAnimation==null)
					fv.gunThread.flashAnimation = Util.getDefaultMuzzleFlashBitmap(AndroidGunz.getCtx());
			}
			
			if(config.hasShootlastAnimation){
				try{
					fireLastAnimation = getAnimation(ANIM_STAGE_SHOOTLAST);
				} catch(Exception e){
					config.hasShootlastAnimation = false;
					fireLastAnimation = null;
				}
			}
			
			fv.setFlashOffset(config.muzzleFlashOffsetX, 
					config.muzzleFlashOffsetY);
			
			GameController.rAnimationFinished = new Runnable() {
				
				public void run() {
					fv.preload(reloadAnimation,true);
					GameController.rAnimationFinished = new Runnable() {
						
						public void run() {
							AndroidGunz.onPreloadFinished();
							fv.preloading = false;
							GameController.rAnimationFinished = null;
						}
					};
				}
			};
			
			fv.preload(fireAnimation,true);
			
			//uncomment to watch preload shooting process
			//AndroidGunz.onPreloadFinished();
	}
	
	protected boolean fire(PointF impact){
		return fire(false,impact);
	}
	
	protected boolean fire(boolean test, PointF impact){
		
		boolean shotFired = false;
		isFiring = true;
		
		if(currentAnimationName!=null && currentAnimationName.equals(ANIM_STAGE_RELOAD)){
			//boolean isFinished = reloadAnimation.getCurrent().equals(reloadAnimation.getFrame(reloadAnimation.getNumberOfFrames()-1));
			if(fv.gunThread.isPlaying())
			{
				if(DBG_MODE)
					Log.w("Firearm","Waiting for reload to finish..");
				isFiring = false;
				return shotFired;
			}
		}
		
		if(fv.gunThread.isPlaying())
		{
			fv.stopAnimation();
			if(DBG_MODE)
				Log.w("Firearm","Stopping animation before firing again");
		}
		
		fv.gunThread.impactOffset = impact;
		
		//reloading weapon
		if(!config.johnWoo && roundsLeft<=0)
		{
			if(config.autoReload || roundsLeft==-1){
				//TODO: show reload anims
				roundsLeft = config.magazineCap;
				currentAnimationName = ANIM_STAGE_RELOAD;
				//AndroidGunz.gunview.setBackgroundDrawable(reloadAnimation);
				fv.setAnimationFrames(reloadAnimation,ANIM_STAGE_RELOAD);
				fv.setGunDischarge(false);
				
				soundPlaylist = new Runnable() {
					public void run() {
						Util.createOneshot(AndroidGunz.getCtx(), getSoundId(ANIM_STAGE_RELOAD),true);
					}
				};
			} else {
				soundPlaylist = new Runnable() {
					
					public void run() {
						Util.createOneshot(AndroidGunz.getCtx(), getSoundId(ANIM_SOUND_SLIDEBACK),true);
					}
				};
				new Thread(soundPlaylist).start();
				isFiring = false;
				return shotFired;
			}
		}
		//shooting last round in the weapon
		else if(roundsLeft==1){
			
			if(config.hasShootlastAnimation){
				currentAnimationName = ANIM_STAGE_SHOOTLAST;
				fv.setAnimationFrames(fireLastAnimation,ANIM_STAGE_SHOOTLAST);
			}
			else{
				currentAnimationName = null;
			}
			
			//AndroidGunz.gunview.setBackgroundDrawable(fireLastAnimation);
			fv.setGunDischarge(true);
			
			soundPlaylist = new Runnable() {
				
				public void run() {
					playSoundBlocking(getSoundId());
					try {
						Thread.sleep(75);
					} catch (InterruptedException e) {
						if(DBG_MODE)
							e.printStackTrace();
					}
					if(config.hasShootlastAnimation)
						Util.createOneshot(AndroidGunz.getCtx(), getSoundId(ANIM_SOUND_SLIDEBACK),true);
				}
			};
			shotFired = true;
			if(!config.johnWoo){
				roundsLeft--;
				if(DBG_MODE)
					Log.i("Firearm","Number of rounds left: "+roundsLeft);
			}
		}
		//shooting normally
		else{
			if(DBG_MODE)
				Log.i("Firearm","Firing..");
			
			currentAnimationName = null;
			//AndroidGunz.gunview.setBackgroundDrawable(fireAnimation);
			fv.setAnimationFrames(fireAnimation,ANIM_STAGE_SHOOT);
			fv.setGunDischarge(true);
			
			soundPlaylist = new Runnable() {
				
				public void run() {
					playSoundBlocking(getSoundId());
				}
			};
			
			shotFired = true;
			if(!config.johnWoo){
				roundsLeft--;
				if(DBG_MODE)
					Log.i("Firearm","Number of rounds left: "+roundsLeft);
			}
		}
		
		fv.startAnimation();
		
		if(!test){
			if(currentAnimationName!=null && currentAnimationName.equals(ANIM_STAGE_RELOAD)){
				new Thread(soundPlaylist).start();
			}
			
			if(!config.johnWoo)
				AndroidGunz.setRoundCount(roundsLeft);
		}
		
		if(shotFired){
			shotCount++;
			shotTimer++;
			if(shotTimer >= AndroidGunz.NAG_SHOTS_ALLOWED){
				shotTimer = 0;
				//AndroidGunz.onNagThreshold();
			}
		}
		
		isFiring = false;
		*/
		/*
		AndroidGunz.mHandler.postDelayed(new Runnable() {
			
			public void run() {
				isFiring = false;
			}
		}, DOUBLETAP_DELAY);*/
		
		//return shotFired;
	//}
	
	/*public static void onDischarge()
	{
		//discharge sound
		if(soundPlaylist!=null){
			new Thread(soundPlaylist).start();
		}
		if(currentAnimationName != ANIM_STAGE_RELOAD)
		{
			//vibrate phone
			t.schedule(new TimerTask() {
				
				public void run() {
					((AndroidGunz)AndroidGunz.getCtx()).getVibrator().vibrate(gunVibrateTime);
				}
			}, 1);
		}
	}*/
	
	
	/*public boolean setAnimation(String stage, int level){
		if(level>gunDischargeAnimsCount){
			if(DBG_MODE)
				Log.e("Firearm","Invalid set animation level for stage: "+stage);
			return false;
		}
		
		currentStage = stage;
		currentLevel = level;
		
		//currentAnimation =  getAnimation(stage,level);
		currentAnimationName = stage;
		return true;
	}*/
	
	/*public AnimationDrawable getAnimation(){
		return AndroidGunz.;
	}*/
	/*
	public Bitmap[] getAnimation(String stage){
		return getAnimation(stage,1);
	}
	public Bitmap[] getAnimation(String stage, int level){
		String ident;
		if(stage.equals(ANIM_STAGE_SHOOT)){
			if(level>gunDischargeAnimsCount)
				level = ANIM_LEVEL_FIRST;
			ident = FireArmOld.DRAWABLE_PREFIX+gunName+"_"+stage+level;
		}
		else{
			ident = FireArmOld.DRAWABLE_PREFIX+gunName+"_"+stage;
		}
		
		int xmlId = AndroidGunz.getCtx().getResources().getIdentifier(ident, "xml", AndroidGunz.getCtx().getPackageName());
		//String[] animationFrames = Util.parseXmlAnimation(AndroidGunz.getCtx(), resId2);
		return Util.getAnimationFrames(AndroidGunz.getCtx(), xmlId);
		
		
		//int resId = AndroidGunz.getCtx().getResources().getIdentifier(ident, "drawable", AndroidGunz.getCtx().getPackageName());
		//return (MyAnimationDrawable) AndroidGunz.getCtx().getResources().getDrawable(resId);
		//return Util.convertAnimationDrawableToBitmap(AndroidGunz.getCtx(), resId);
	}
	
	public int getSoundId(){
		return getSoundId(currentStage,currentLevel);
	}
	public int getSoundId(String stage){
		return getSoundId(stage,0);
	}
	public int getSoundId(String stage, int level){
		
		String ident;
		if(stage.equals(ANIM_STAGE_SHOOT)){
			if(level>gunDischargeAnimsCount)
				level = ANIM_LEVEL_FIRST;
			ident =  gunName+"_"+stage+level;
		}
		else
			ident =  gunName+"_"+stage;
		
		return AndroidGunz.getCtx().getResources().getIdentifier(ident, "raw", AndroidGunz.getCtx().getPackageName());
	}
	
	public static FirearmBio getBio(Context c, String weaponId){
		FirearmBio fb = new FirearmBio();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			String ident = FireArmOld.BIOXML_PREFIX+weaponId;
			int resId = c.getResources().getIdentifier(ident, "raw", c.getPackageName());
			Document document = builder.parse(c.getResources().openRawResource(resId));
			fb.actionType = document.getElementsByTagName(BIO_TAGNAME_ACTION).item(0).getFirstChild().getNodeValue();
			fb.cartdrigeName = document.getElementsByTagName(BIO_TAGNAME_CNAME).item(0).getFirstChild().getNodeValue();
			fb.cartdrigeVelocity = document.getElementsByTagName(BIO_TAGNAME_CFPS).item(0).getFirstChild().getNodeValue();
			fb.cartridgePower = document.getElementsByTagName(BIO_TAGNAME_CPOWER).item(0).getFirstChild().getNodeValue();
			fb.cartridgeRange = document.getElementsByTagName(BIO_TAGNAME_CRANGE).item(0).getFirstChild().getNodeValue();
			fb.description = document.getElementsByTagName(BIO_TAGNAME_DESC).item(0).getFirstChild().getNodeValue();
			fb.designedYear = document.getElementsByTagName(BIO_TAGNAME_YEAR).item(0).getFirstChild().getNodeValue();
			fb.designer = document.getElementsByTagName(BIO_TAGNAME_AUTHOR).item(0).getFirstChild().getNodeValue();
			fb.fireRate = document.getElementsByTagName(BIO_TAGNAME_ROF).item(0).getFirstChild().getNodeValue();
			fb.history = document.getElementsByTagName(BIO_TAGNAME_HISTORY).item(0).getFirstChild().getNodeValue();
			fb.length = document.getElementsByTagName(BIO_TAGNAME_LENGTH).item(0).getFirstChild().getNodeValue();
			fb.lengthBarrel = document.getElementsByTagName(BIO_TAGNAME_LENGTHB).item(0).getFirstChild().getNodeValue();
			fb.usedBy = document.getElementsByTagName(BIO_TAGNAME_USEDBY).item(0).getFirstChild().getNodeValue();
			fb.usedWars = document.getElementsByTagName(BIO_TAGNAME_USEDWAR).item(0).getFirstChild().getNodeValue();
			fb.weight = document.getElementsByTagName(BIO_TAGNAME_WEIGHT).item(0).getFirstChild().getNodeValue();
			fb.feed = document.getElementsByTagName(BIO_TAGNAME_FEED).item(0).getFirstChild().getNodeValue();
		} catch(Exception e){
			
		}
		return fb;
	}
	
	public void parseConfig(){
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e1) {
			if(DBG_MODE)
				e1.printStackTrace();
			builder = null;
		}
		
		if(builder==null)
			return;
		
		try {
			String ident = FireArmOld.CONFIG_PREFIX+gunName;
			int resId = AndroidGunz.getCtx().getResources().getIdentifier(ident, "raw", AndroidGunz.getCtx().getPackageName());
			Document document = builder.parse(AndroidGunz.getCtx().getResources().openRawResource(resId));
			
			String magCapacity = document.getElementsByTagName(CONFIG_TAGNAME_MAGAZINECAPACITY).item(0).getFirstChild().getNodeValue();
			String dischargeAnimations = document.getElementsByTagName(CONFIG_TAGNAME_DISCHARGEANIMS).item(0).getFirstChild().getNodeValue();
			String mFlashOffsetX = document.getElementsByTagName(CONFIG_TAGNAME_MFLASHOFFSETX).item(0).getFirstChild().getNodeValue();
			String mFlashOffsetY = document.getElementsByTagName(CONFIG_TAGNAME_MFLASHOFFSETY).item(0).getFirstChild().getNodeValue();
			String mFlashEnabled = document.getElementsByTagName(CONFIG_TAGNAME_MFLASHENABLED).item(0).getFirstChild().getNodeValue();
			
			if(DBG_MODE){
				Log.i("Firearm","Loading config");
				Log.i("Firearm","Found magazine capacity: "+magCapacity);
				Log.i("Firearm","Found discharge anims: "+dischargeAnimations);
				Log.i("Firearm","Found muzzle flash offset x: "+mFlashOffsetX);
				Log.i("Firearm","Found muzzle flash offset y: "+mFlashOffsetY);
			}
			
			config.magazineCap 		  = Integer.parseInt(magCapacity);
			config.dischareAnimations = Integer.parseInt(dischargeAnimations);
			config.muzzleFlashOffsetX = Integer.parseInt(mFlashOffsetX);
			config.muzzleFlashOffsetY = Integer.parseInt(mFlashOffsetY);
			config.hasMuzzleFlash 	  = mFlashEnabled.equals("false")?false:true;
			
			//restore saved options
			config.johnWoo = MenuActivity.getOptions(AndroidGunz.getCtx()).getSwitch(Options.PREF_KEY_OPTION_JOHNWOO);
			
		} catch (Exception e) {
			if(DBG_MODE)
				e.printStackTrace();
			factory = null;
		}
		factory = null;
	}
	
	private boolean playSoundBlocking(int res){
    	
    	int x = 1;
    	for (MediaPlayer mpObj : mpPool) {
    		
    		if(mpObj!=null && mpObj.isPlaying()){
    			if(DBG_MODE)
        			Log.i("playSoundBlocking","mediaplyer #"+x+" is busy, checking next mp..");
        		continue;
        	}
    		
    		if(mpObj != null){
    			if(DBG_MODE)
        			Log.i("playSoundBlocking","releasing mediaplyer #"+x+"..");
    			mpObj.release();
        	}
    		
    		mpObj = MediaPlayer.create(AndroidGunz.getCtx(), res);
    		if(mpObj!=null)
    		{
	    		mpObj.setOnCompletionListener(AndroidGunz.mpComplete);
	    		mpObj.setOnErrorListener(AndroidGunz.mpError);
	    		mpObj.start();
    		}
    		return true;
		}
    	return false;
    }

	public boolean isReady() {
		return ready;
	}

	public  void setJohnWooMode(boolean enable){
		config.johnWoo = enable;
	}
	public  void setAutoReload(boolean enable){
		config.autoReload = enable;
	}

	public void setEnableHeadshots(boolean enableHeadshots) {
		this.enableHeadshots = enableHeadshots;
	}

	public boolean isEnableHeadshots() {
		return enableHeadshots;
	}

	public void setEnableFaceDetection(boolean enableFaceDetection) {
		this.enableFaceDetection = enableFaceDetection;
	}

	public boolean isEnableFaceDetection() {
		return enableFaceDetection;
	}

	public void setEnableHitboxes(boolean enableHitboxes) {
		this.enableHitboxes = enableHitboxes;
	}

	public boolean isEnableHitboxes() {
		return enableHitboxes;
	}

	public void setFiring(boolean isFiring) {
		this.isFiring = isFiring;
	}

	public boolean isFiring() {
		return isFiring;
	}
	*/
}
