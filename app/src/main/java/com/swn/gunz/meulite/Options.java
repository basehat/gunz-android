package com.swn.gunz.meulite;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class Options {
	private static boolean DBG_MODE = false;
	
	private static final String PREF_FILE = "settings.dat";
	
	protected static final String PREF_KEY_OPTION_JOHNWOO = "pkojw";
	protected static final String PREF_KEY_OPTION_AUTORELOAD = "pkoar";
	protected static final String PREF_KEY_OPTION_MUSIC = "pkmusic";
	protected static final String PREF_KEY_OPTION_MUSIC_REPEAT = "pkmrp";
	protected static final String PREF_KEY_OPTION_FACEDETECT = "pkfd";
	protected static final String PREF_KEY_OPTION_HITBOXES = "pkhitbox";
	protected static final String PREF_KEY_OPTION_HEADSHOTS = "pkheadshot";
	
	protected static final String MUSIC_TRACK_NONE = "";
	protected static final String MUSIC_TRACK_007 = "007";
	protected static final String MUSIC_TRACK_HALO = "halo";
	protected static final String MUSIC_TRACK_SPY = "spy1";
	protected static final String MUSIC_TRACK_SPY2 = "spy2";
	protected static final String MUSIC_RESOURCE_PREFIX = "music_";
	
	private SharedPreferences userConfig;
	
	public Options(Context c){
		userConfig = c.getApplicationContext().getSharedPreferences(PREF_FILE, 0);
	}
	
	public void setSwitch(String key, boolean val){
		try{
    		SharedPreferences.Editor editor = userConfig.edit();
    		editor.putBoolean(key,val);
    		editor.commit();
    	} catch(Exception e){
    		if(DBG_MODE)
    			Log.e("Options","error commiting pref changes: "+e);
    	}
	}
	
	public boolean getSwitch(String key){
		return userConfig.getBoolean(key,false);
	}
	public boolean getSwitch(String key, boolean def){
		return userConfig.getBoolean(key, def);
	}
	
	public void setUserConfig(SharedPreferences userConfig) {
		this.userConfig = userConfig;
	}

	public SharedPreferences getUserConfig() {
		return userConfig;

	}
	public boolean setString(String key, String value){
		boolean ret = false;
		try{
    		SharedPreferences.Editor editor = userConfig.edit();
    		editor.putString(key, value);
    		ret = editor.commit();
    	} catch(Exception e){
    		if(DBG_MODE)
    			Log.e("Options","error commiting pref changes: "+e);
    	}
    	return ret;
	}
	public String getString(String key){
		return getString(key,"");
	}
	public String getString(String key,String def){
		return userConfig.getString(key, def);
	}
}
