package com.swn.gunz.meulite;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

//SKT
import com.skt.arm.aidl.IArmService;

public class SplashActivity extends Activity {

	private Handler mHandler = new Handler();
	private static final int animSectionTime = 400;
	protected static final String wlLabel = "SplashScreen";
	protected PowerManager.WakeLock wl;
	private MediaPlayer mpSound3;
	private Intent fwdIntent;
	
	private static final int DLG_SKT = 0;
	
	//skt
	private IArmService service;
	private ArmServiceConnection armCon;
	private int arm_res = 1;
	private String AID = "OA00054842";
	
	private void setTextStyle(FontView v){
		v.setFont(FontView.FONT_TOKYOSOFT);
		v.setShadowLayer(5, 0, 0, Color.parseColor("#20caee"));
		v.setTextColor(Color.BLACK);
		v.setPadding(5, 0, 5, 0);
	}
	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setTheme(android.R.style.Theme_Light_NoTitleBar_Fullscreen);
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		 
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
	    		WindowManager.LayoutParams.FLAG_FULLSCREEN); 
	    
        setContentView(R.layout.splash);
        
        //PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        /*wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK|
        						PowerManager.ACQUIRE_CAUSES_WAKEUP|
        						PowerManager.ON_AFTER_RELEASE,
        					wlLabel);
        wl.acquire(5000);*/
        
        if(AndroidGunz.ENABLE_SKT)
        {
	        if(!runService()){
	        	arm_res = 0;
	        	showDialog(DLG_SKT);
	        	return;
	    	}
        }
        
        if(AndroidGunz.ENABLE_SKT)
        {
        	new Thread(new Runnable() {
				public void run() {
					mHandler.post(new Runnable() {
						public void run() {
							ImageView i1 = (ImageView)findViewById(R.id.skt_rating);
							ImageView i2 = (ImageView)findViewById(R.id.skt_rating2);
							i1.setImageResource(R.drawable.skt1);
							i2.setImageResource(R.drawable.skt2);
						}
					});
					
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e) {
						return;
					}
					mHandler.post(new Runnable() {
						public void run() {
							ImageView i1 = (ImageView)findViewById(R.id.skt_rating);
							ImageView i2 = (ImageView)findViewById(R.id.skt_rating2);
							Animation a = AnimationUtils.loadAnimation(SplashActivity.this,
									android.R.anim.fade_out);
							a.setFillAfter(true);
							a.setDuration(700);
							i1.startAnimation(a);
							i2.startAnimation(a);
						}
					});
				}
			}).start();
        }
        
        AudioManager am = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        am.setStreamMute(AudioManager.STREAM_MUSIC, false);
        am.setStreamVolume(AudioManager.STREAM_MUSIC, 
				am.getStreamMaxVolume(AudioManager.STREAM_MUSIC),0);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        
        mpSound3 = MediaPlayer.create(SplashActivity.this, R.raw.wrench);
        if(mpSound3!=null)
        {
	        mpSound3.setOnCompletionListener(new OnCompletionListener() {
	    		
	    		public void onCompletion(MediaPlayer mp) {
	    			mp.release();
	    		}
	    	});
        }
        
        final FontView iv  = (FontView)findViewById(R.id.splash_a);
        final FontView iv2 = (FontView)findViewById(R.id.splash_b);
        final FontView iv3 = (FontView)findViewById(R.id.splash_c);
        final FontView iv4 = (FontView)findViewById(R.id.splash_d);
        final FontView iv5 = (FontView)findViewById(R.id.splash_e);
        final FontView iv6 = (FontView)findViewById(R.id.splash_f);
        final FontView iv7 = (FontView)findViewById(R.id.splash_g);
        final FontView iv8 = (FontView)findViewById(R.id.splash_h);
        
        setTextStyle(iv);
        setTextStyle(iv2);
        setTextStyle(iv3);
        setTextStyle(iv4);
        setTextStyle(iv5);
        setTextStyle(iv6);
        setTextStyle(iv7);
        setTextStyle(iv8);
        
        final Animation a = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);
        final Animation b = AnimationUtils.loadAnimation(this, R.anim.rotate_ccw_90);
        final Animation c = AnimationUtils.loadAnimation(this, R.anim.rotate);
        
        c.setInterpolator(this, R.anim.linear_interpolator);
        //c.setRepeatCount(20);
        //c.setRepeatMode(Animation.RESTART);
        
        b.setAnimationListener(new Animation.AnimationListener() {
			
			public void onAnimationStart(Animation animation) {
			}
			
			public void onAnimationRepeat(Animation animation) {
			}
			
			public void onAnimationEnd(Animation animation) {
				mHandler.postDelayed(new Runnable() {
					
					public void run() {
						//Intent in = new Intent(SplashActivity.this, MenuActivity.class);
						//in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						//Intent i = new Intent(SplashActivity.this,AndroidGunz.class);
						//startActivity(in);
						if(AndroidGunz.ENABLE_SKT)
						{
							if(arm_res != 1){
								return;
							}
						}
						startActivity(fwdIntent);
					}
				}, 500);
			}
		});
        
        fwdIntent = new Intent(SplashActivity.this, MenuActivity.class);
		//startActivity(fwdIntent);
        
        c.setAnimationListener(new Animation.AnimationListener() {
			
			public void onAnimationStart(Animation animation) {
			}
			
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
			}
			
			public void onAnimationEnd(Animation animation) {
				iv8.clearAnimation();
				iv8.startAnimation(b);
			}
		});
        
        //c.setFillAfter(true);
        b.setFillAfter(true);
        
        a.setAnimationListener(new Animation.AnimationListener() {
			
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
			}
			
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
			}
			
			public void onAnimationEnd(Animation animation) {
				MediaPlayer tmpPlayer = MediaPlayer.create(SplashActivity.this, 
											R.raw.thump);
				if(tmpPlayer==null)
					return;
				tmpPlayer.setOnCompletionListener(new OnCompletionListener() {
		    		
		    		public void onCompletion(MediaPlayer mp) {
		    			mp.release();
		    		}
		    	});
				tmpPlayer.start();
			}
		});
        
        iv8.setVisibility(View.VISIBLE);
        iv8.startAnimation(a);
 
        int i =1;
        
        mHandler.postDelayed(new Runnable() {
			
			public void run() {
				iv7.setVisibility(View.VISIBLE);
				iv7.startAnimation(a);
			}
		}, animSectionTime*i);
        i++;
        
        mHandler.postDelayed(new Runnable() {
			
			public void run() {
				iv6.setVisibility(View.VISIBLE);
				iv6.startAnimation(a);
			}
		}, animSectionTime*i);
        i++;
        
        mHandler.postDelayed(new Runnable() {
			
			public void run() {
				iv5.setVisibility(View.VISIBLE);
				iv5.startAnimation(a);
			}
		}, animSectionTime*i);
        i++;
        
        mHandler.postDelayed(new Runnable() {
			
			public void run() {
				iv4.setVisibility(View.VISIBLE);
				iv4.startAnimation(a);
			}
		}, animSectionTime*i);
        i++;
        
        mHandler.postDelayed(new Runnable() {
			
			public void run() {
				iv3.setVisibility(View.VISIBLE);
				iv3.startAnimation(a);
			}
		}, animSectionTime*i);
        i++;
        
        mHandler.postDelayed(new Runnable() {
			
			public void run() {
				iv2.setVisibility(View.VISIBLE);
				iv2.startAnimation(a);
			}
		}, animSectionTime*i);
        i++;
        
        mHandler.postDelayed(new Runnable() {
			
			public void run() {
				iv.setVisibility(View.VISIBLE);
				a.setAnimationListener(new Animation.AnimationListener() {
					
					public void onAnimationStart(Animation animation) {
						// TODO Auto-generated method stub
					}
					
					
					public void onAnimationRepeat(Animation animation) {
						// TODO Auto-generated method stub
					}
					
					public void onAnimationEnd(Animation animation) {
						MediaPlayer tmpPlayer = MediaPlayer.create(SplashActivity.this, R.raw.thump);
						tmpPlayer.setOnCompletionListener(AndroidGunz.mpComplete);
						tmpPlayer.start();
						if(mpSound3!=null){
							try{
								mpSound3.start();
							}catch(IllegalStateException e){
							}
						}
						mHandler.postDelayed(new Runnable() {
							
							public void run() {
								iv8.clearAnimation();
								iv8.startAnimation(c);
							}
						}, 450);
					}
				});
				iv.startAnimation(a);
			}
		}, animSectionTime*i);
        i++;
        
	}
	
	public void onWindowFocusChanged (boolean hasFocus) {
		if(AndroidGunz.ENABLE_SKT)
		{
			if((hasFocus) && (arm_res != 1)){
				close();
			}
		}
	}
	
	protected Dialog onCreateDialog(int id) {
		Dialog d = null;
		if(AndroidGunz.ENABLE_SKT)
		{
			if(id==DLG_SKT)
			{
				d =  new AlertDialog.Builder(this)
					.setTitle(R.string.classarm_license_check)
					.setMessage(getARMMessage(arm_res))
					.setPositiveButton(R.string.kr_ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						close();// When push the  the program will be closed.
					}
					}).create();
			}
		}
		return d;
	}
	
	protected void onPause() {
		//wl.release();
		super.onPause();
	}
	
	private String getARMMessage(int id) {
		switch(id) {
			case 0:
			return getString(R.string.err_arm);
			case 1:
			return getString(R.string.err_01);
			case 0xF0000004:
			return getString(R.string.err_04);
			case 0xF0000008:
			return getString(R.string.err_08);
			case 0xF0000009:
			return getString(R.string.err_09);
			case 0xF000000A:
			return getString(R.string.err_0a);
			case 0xF000000C:
			return getString(R.string.err_0c);
			case 0xF000000D:
			return getString(R.string.err_0d);
			case 0xF000000E:
			return getString(R.string.err_0e);
			case 0xF0000011:
			return getString(R.string.err_11);
			case 0xF0000012:
			return getString(R.string.err_12);
			case 0xF0000013:
			return getString(R.string.err_13);
			case 0xF0000014:
			return getString(R.string.err_14);
		}
			return getString(R.string.err_arm);
		}
		
		private void close() {
			this.finish();
		}
		
		private boolean runService(){
			if(AndroidGunz.ENABLE_SKT)
			{
				try
				{
					if(armCon == null)
					{
						armCon = new ArmServiceConnection(); boolean conRes = bindService(new Intent(IArmService.class.getName()), armCon, Context.BIND_AUTO_CREATE); //----(1)
						if(conRes)
							return true;
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				releaseService();
			}
			return false;
		}
		
		private void releaseService(){
			if(AndroidGunz.ENABLE_SKT)
			{
				if(armCon != null)
				{ 
					unbindService(armCon); //----(4)
					armCon = null;
					service = null;
				}
			}
		}
		
		class ArmServiceConnection implements ServiceConnection
		{
			public void onServiceConnected(ComponentName name, IBinder boundService) //----(2)
			{
				if(service == null)
					service = IArmService.Stub.asInterface((IBinder) boundService);
				
				try{
				// in case of success
				// Application is properly executed
				arm_res = service.executeArm(AID); //----(3)
				if(arm_res != 1) {
				showDialog(DLG_SKT);
				}
				}catch(Exception e){
				e.printStackTrace();
				releaseService();
				arm_res = 0;
				showDialog(DLG_SKT);
				return;
				}
				releaseService();
				}
				public void onServiceDisconnected(ComponentName name) {
				service = null;
			}
		}
}
