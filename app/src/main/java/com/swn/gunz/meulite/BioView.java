package com.swn.gunz.meulite;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class BioView extends SurfaceView implements SurfaceHolder.Callback {

	public static final int DEFAULT_BG_COLOR = Color.RED;
	
	public static final int FRAME_DELAY = 75;
	
	public BioThread bioThread = null;
	private boolean surfaceReady = false;
	private String currentAnimationName = null;
	
	public BioView(Context context, AttributeSet attrs) {
		super(context, attrs);
		SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        
        bioThread = new BioThread(holder, context, null);
        
        setFocusable(true);
	}
	
	public void startAnimation()
	{
		bioThread.animationStopped = false;
	}
	
	public void stopAnimation()
	{
		bioThread.animationStopped = true;
	}

	public void setAnimationFrames(Bitmap[] b, String name)
	{
		if(currentAnimationName!=null && currentAnimationName.equals(name)){
			return;
		}
		
		currentAnimationName = name;
		
		bioThread.currentAnimation = null;
		bioThread.currentAnimation = b;
		//bioThread.bgColor = b[0].getPixel(5, 5);
	}
	
	
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		
	}

	
	public void surfaceCreated(SurfaceHolder holder) {
		
		surfaceReady = true;
		bioThread.mSurfaceHolder = holder;
		bioThread.mRun = true;
		bioThread.isSurfaceReady = true;
		
		if(!bioThread.started){
			bioThread.start();
		}
	}

	
	public void surfaceDestroyed(SurfaceHolder holder) {
		bioThread.isSurfaceReady = false;
		surfaceReady = false;
	}
	
	public void destroy() {
		bioThread.mRun = false;
		bioThread.interrupt();
		bioThread.isSurfaceReady = false;
		surfaceReady = false;
		
		if(bioThread.currentAnimation!=null)
		{
			for (Bitmap b : bioThread.currentAnimation) {
				b.recycle();
				b = null;
			}
			bioThread.currentAnimation = null;
		}
		
		currentAnimationName = null;
	}
	
	class BioThread extends Thread {
		public SurfaceHolder mSurfaceHolder;
        public boolean isSurfaceReady = false;
        public Bitmap[] currentAnimation;
        private Paint mPaint;
        public boolean animationStopped = true;
        public Canvas mainCanvas = null;
        public int marginTop = 0;
        public boolean mRun = false;
        public boolean started = false;
        public int lastFrame = 0;
        public int bgColor = BioView.DEFAULT_BG_COLOR;
        
        public BioThread(SurfaceHolder surfaceHolder, Context context, Bitmap[] anim) {
        	
        	mSurfaceHolder = surfaceHolder;
            currentAnimation = anim;
            
            mPaint = new Paint();
			mPaint.setAntiAlias(true);
			mPaint.setDither(false);
            
			//mSurfaceHolder.setFormat(PixelFormat.RGBA_8888);
        }
        
        private void drawToCanvas() throws InterruptedException
        {
        	if(currentAnimation!=null)
        	{
        		if(currentAnimation.length>0)
        		{
        			int x = currentAnimation.length;
        			for(int i=lastFrame;i<=x;i++)
        			{
        				if(animationStopped){
        					return;
        				}
        				
        				if(i==x)
        					i = 0;
        				
        				lastFrame = i;
        				
        				mainCanvas = mSurfaceHolder.lockCanvas(null);
        				if(mainCanvas==null)
        					return;
        				
        				mainCanvas.drawColor(Color.BLACK);
        				
        				mainCanvas.drawBitmap(currentAnimation[i], 0.0f, this.marginTop, mPaint);
        				mSurfaceHolder.unlockCanvasAndPost(mainCanvas);
        				mainCanvas = null;
                		
                		if(animationStopped){
        					return;
        				}
                		
        				Thread.sleep(FRAME_DELAY);
        				
        			}
        		}
        	}
        }
        
        
        public void run() {
        	
        	currentAnimation = Util.getAnimationFrames(getContext(), R.xml.x_meu_flyby);
        	
        	started = true;
        	bioThread.animationStopped = false;
        	while(mRun)
        	{
        		if(!isSurfaceReady){
        			try {
						Thread.sleep(250);
					} catch (InterruptedException e) {
					}
        			continue;
        		}
        		
        		if(animationStopped){
        			continue;
        		}
        		
        		try {
                    synchronized (mSurfaceHolder) {
                    	try{
                    		drawToCanvas();
                    	} 
                    	catch(InterruptedException e)
                    	{
                    		if (mainCanvas != null) {
                    			mSurfaceHolder.unlockCanvasAndPost(mainCanvas);
                                mainCanvas = null;
                            } else {
                            }
                    	}
                    }
                } finally {
                }
                
        	}
        	
        }
        
	}
	
}
