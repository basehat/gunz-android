package com.swn.gunz.meulite;

import android.graphics.Bitmap;
import android.net.Uri;

public class TargetSource {
	
	protected int resourceID = 0;
	protected Bitmap bitmapObject = null;
	protected Uri resourceUri = null;
	
	public TargetSource() {
		
	}
	
	public TargetSource(int resourceId) {
		this.resourceID = resourceId;
	}
	
	public TargetSource(Bitmap bitmap) {
		this.bitmapObject = bitmap;
	}
	
	public TargetSource(Uri imageUri) {
		this.resourceUri = imageUri;
	}
	
	public void clearData(){
		this.resourceID = 0;
		this.bitmapObject = null;
		this.resourceUri = null;
	}

}
