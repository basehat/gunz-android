package com.swn.gunz.meulite;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.HashSet;

import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlManager;
import com.adwhirl.AdWhirlTargeting;
import com.adwhirl.AdWhirlLayout.AdWhirlInterface;
import com.adwhirl.adapters.AdWhirlAdapter;
import com.adwhirl.util.AdWhirlUtil;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/*import com.google.ads.AdSenseSpec;
import com.google.ads.AdViewListener;
import com.google.ads.GoogleAdView;
import com.google.ads.AdSenseSpec.AdFormat;
import com.google.ads.AdSenseSpec.AdType;
import com.google.ads.AdSenseSpec.ExpandDirection;*/


public class AndroidGunz extends Activity implements AdWhirlInterface {
	
	public static final int GUN_ID = 1;
	
	public static final boolean HAS_GOOGLE_MARKET = false;
	public static final boolean ENABLE_SKT = false;
	
	public static final int MAX_ANIMATION_FRAMES = 64;//per xml file
	
	public static final long INITIAL_HEAP_SIZE = 4194304;
	
	public static final String ADSENSE_ID = "ca-mb-app-pub-8341036752058942";
	public static final String ADWHIRL_SDK_KEY = "65cf9c110d1b4025bb4e071d5dea1e67";
	public final static boolean DBG_MODE = false;
	public final static boolean LITE_MODE = false;
	
	public final static int NAG_SCREEN_TIME = 5000;
	public final static int NAG_SHOTS_ALLOWED = 50;
	
	public final static int ACTIVITY_RESULT_SELECTPHOTO = 890;
	public final static int ACTIVITY_RESULT_OOM = 891;
	
	public static final int TARGET_MODE_DEFAULT = 0;
	public static final int TARGET_MODE_ANIMATED = 1;
	public static final int TARGET_MODE_ANIMATED2 = 2;
	
	public static final String INTENT_EXTRA_GAMEMODE = "gunz_mode";
	
	private RelativeLayout mainLayout;
	
	//private static GoogleAdView adView = null; 
	
	protected GameController gameView;
	
	//protected static ImageView gunview2;
	protected static ImageView bMenu;
	//public static TargetLayout target0;
	protected static Handler mHandler;
	protected static TextView roundCount;
	//protected static Target mainTarget = null;
	private static RelativeLayout loadingScreen;
	//private static Thread loadingScreenController = null;
	protected static MediaPlayer musicPlayer;
	protected static boolean	 isMusicPlaying = false;
	private static Context ctx;
	private static boolean loadingScreenOn = false;
	private static boolean adScreenOn = false;
	private Vibrator vibrator;
	private static int currentScore = 0;
	private static int finalScore = 0;
	private static RelativeLayout roundView;
	
	private Bitmap importedImage = null;
	
	protected static Typeface arFont;
	
	private static TextView scoreView;
	
	private static boolean adWhirlOn = false;
	
	private AdWhirlLayout adWhirlLayout = null;
	
	private Menus menuController;
	
	//protected static Dialog gameMenu;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.main);
        
        ctx = this;
        
        mHandler = new Handler();
        
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        
        menuController = new Menus(this);
        
        //isMusicPlaying = false;
        
        bMenu = (ImageView)findViewById(R.id.b_menu);
        
        if(mainLayout==null)
        	mainLayout = (RelativeLayout) findViewById(R.id.mainframe);
        
        if(roundCount==null)
        	roundCount = (TextView)findViewById(R.id.round_count);
        
        if(loadingScreen==null) {
        	loadingScreen = (RelativeLayout)findViewById(R.id.loadingoverlay);
        	//loadingScreen.setWillNotCacheDrawing(true);
        }
        
        if(roundView==null){
        	roundView = (RelativeLayout)findViewById(R.id.round_display);
        }
        
        if(vibrator==null)
        	setVibrator((Vibrator)getSystemService(VIBRATOR_SERVICE));
        
        
        adWhirlLayout = (AdWhirlLayout)findViewById(R.id.ad_con);
        
        if(LITE_MODE)
        {
        	adWhirlLayout.setBackgroundColor(Color.TRANSPARENT);
        	adWhirlLayout.setAdWhirlInterface(this);
        } else {
        	adWhirlLayout.setVisibility(View.INVISIBLE);
        	mainLayout.removeView(adWhirlLayout);
        }
        
        int inGameMode = getIntent().getIntExtra(INTENT_EXTRA_GAMEMODE, 
				TARGET_MODE_DEFAULT);
        
        GameController.setTargetMode(inGameMode);
        
        gameView = (GameController)findViewById(R.id.gameview);
        gameView.setEnableFaceDetection(MenuActivity.getOptions(this).getSwitch(Options.PREF_KEY_OPTION_FACEDETECT,false));
        gameView.setEnableHeadshots(MenuActivity.getOptions(this).getSwitch(Options.PREF_KEY_OPTION_HEADSHOTS,false));
        gameView.setEnableHitboxes(MenuActivity.getOptions(this).getSwitch(Options.PREF_KEY_OPTION_HITBOXES,false));
        
        bMenu.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showDialog(Menus.MENU_MAIN);
			}
		});
        
        roundCount.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction()==MotionEvent.ACTION_DOWN)
				{
					//currentWeapon.roundsLeft = 0;
					//currentWeapon.fire(new PointF(0,0));
				}
				return true;
			}
		});
        
        gameView.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction()==MotionEvent.ACTION_DOWN)
				{
					gameView.fireWeapon(new PointF(event.getX(),event.getY()));
					return true;
				}
				return false;
			}
		});
        
        mainLayout.post(new Runnable() {
			public void run() {
				gameView.preload();
				if(LITE_MODE)
					adWhirlLayout.bringToFront();
				else
					adWhirlLayout.setVisibility(View.GONE);
			}
		});
        
        arFont = Typeface.createFromAsset(AndroidGunz.getCtx().getAssets(), "fonts/armalite.ttf");
        
        scoreView = (TextView)findViewById(R.id.b_score);
        scoreView.setTypeface(arFont);
        scoreView.setTextColor(Color.RED);
        scoreView.setTextSize(25.0f);
    }
    
    public void onConfigParsed()
    {
    	if(GameController.config.johnWoo){
        	AndroidGunz.setRoundCount(ctx.getString(R.string.symbol_infinity));
        } else {
        	AndroidGunz.setRoundCount(GameController.config.magazineCap);
        }
    }
    
    public static void updateScore(int amount)
    {
    	finalScore = finalScore+amount;
    	int skip = 1;
    	int pamount = Math.abs(amount);
    	
    	if(pamount <= 100){
    		skip = 2;
    	} else if(pamount <= 500) {
    		skip = 4;
    	} else {
    		skip = 10;
    	}
    	
    	int count = pamount/skip;
    	
    	//score increases
    	if(amount>0)
		{
    		boolean b1 = false;
    		for(int x =0;x<=count;x++){
    			currentScore += skip;
    			if(currentScore>finalScore){
    				currentScore = finalScore;
    				b1 = true;
    			}
    			
    			mHandler.post(new ParamRunnable((Integer)currentScore) {
    				
    				public void run() {
    					int val = (Integer)getParam();
    					scoreView.setText(NumberFormat.getInstance().format(val));
    					scoreView.invalidate();
    				}
    			});
    			
    			if(b1)
    				break;
    		}
		}
    	//score decreases
    	else if(amount<0){
    		boolean b1 = false;
    		for(int x =0;x<=count;x++){
    			currentScore -= skip;
    			if(currentScore<finalScore){
    				currentScore = finalScore;
    				b1 = true;
    			}
    			
    			mHandler.post(new ParamRunnable((Integer)currentScore) {
    				
    				public void run() {
    					int val = (Integer)getParam();
    					scoreView.setText(NumberFormat.getInstance().format(val));
    					scoreView.invalidate();
    				}
    			});
    			
    			if(b1)
    				break;
    		}
    	}
    }
    
    /*
    public static boolean showAd(){
    	
    	if(!LITE_MODE)
    		return false;
    	
    	adScreenOn = true;
    	
    	if(adView==null){
    		adView = (GoogleAdView) ((AndroidGunz)ctx).findViewById(R.id.adview);
    		adView.setAdViewListener(new AdViewListener() {
				
				public void onStartFetchAd() {
				}
				
				public void onFinishFetchAd() {
				}
				
				public void onClickAd() {
					hideAd();
				}
			});
    	}
    	
    	RelativeLayout ll = (RelativeLayout) ((AndroidGunz)ctx).findViewById(R.id.ad_con);
    	final TextView adCaption = (TextView) ((AndroidGunz)ctx).findViewById(R.id.adview_caption);
    	final Animation aNagFinish = AnimationUtils.loadAnimation(ctx, android.R.anim.fade_out);
    	final Button buttonCancel = (Button)((AndroidGunz)ctx).findViewById(R.id.b_ad_cancel);
    	final TextView roundCount = (TextView)((AndroidGunz)ctx).findViewById(R.id.rounds_gcount);
    	
    	ll.setBackgroundResource(R.drawable.adbg);
    	ll.setVisibility(View.VISIBLE);
    	ll.bringToFront();
    	pauseMusicPlayer();
    	
    	adView.bringToFront();
    	adView.showAds(updateAdsenseSpec());
    	
    	buttonCancel.setVisibility(View.GONE);
    	adCaption.setVisibility(View.VISIBLE);
    	adCaption.bringToFront();
		adCaption.setTypeface(Typeface.MONOSPACE);
    	roundCount.setTypeface(arFont);
    	roundCount.setText("You Have Shot\n"+String.valueOf(currentWeapon.getShotCount())+"\nBullets!");
    	roundCount.bringToFront();
    	
    	aNagFinish.setAnimationListener(new Animation.AnimationListener() {
			
			public void onAnimationStart(Animation animation) {
			}
			
			public void onAnimationRepeat(Animation animation) {
			}
			
			public void onAnimationEnd(Animation animation) {
				adCaption.setVisibility(View.INVISIBLE);
				buttonCancel.setVisibility(View.VISIBLE);
				buttonCancel.bringToFront();
			}
		});
    	
    	new Thread(new Runnable() {
			
			public void run() {
				try{
					Thread.sleep(NAG_SCREEN_TIME);
					mHandler.post(new Runnable() {
						
						public void run() {
							adCaption.startAnimation(aNagFinish);
						}
					});
				} catch(InterruptedException e){
					
				}
			}
		}).start();
    	
    	buttonCancel.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				hideAd();
			}
		});
    	
    	return true;
    }
    
    
    public static void hideAd(){
    	RelativeLayout ll = (RelativeLayout) ((AndroidGunz)ctx).findViewById(R.id.ad_con);
    	ll.setVisibility(View.INVISIBLE);
    	adScreenOn = false;
    	startMusicPlayer();
    }
    */
    
    public static boolean resumeMusicPlayer(){
    	if(musicPlayer != null)
    	{
	    	try{
	    		musicPlayer.start();
	    		return true;
	    	} catch(IllegalStateException e){
	    		return false;
	    	}
    	}
    	return false;
    }
    
    public static void startMusicPlayer(){
    	String musicTrack = MenuActivity.getOptions(ctx).getString(Options.PREF_KEY_OPTION_MUSIC);
        
    	stopMusicPlayer();
    	
    	if(musicTrack.equals("")){
        	musicPlayer = null;
        } else {
        	boolean musicLoop = MenuActivity.getOptions(ctx).getSwitch(Options.PREF_KEY_OPTION_MUSIC_REPEAT,true);
        	int resId = ctx.getResources().getIdentifier(Options.MUSIC_RESOURCE_PREFIX+musicTrack, "raw", ctx.getPackageName());
        	
        	musicPlayer = MediaPlayer.create(ctx, resId);
        	if(musicPlayer==null)
        		return;
            musicPlayer.setLooping(musicLoop);
            musicPlayer.start();
            isMusicPlaying = true;
        }
    }
    
    public static void stopMusicPlayer(){
    	stopMusicPlayer(false);
    }
    
    public static void pauseMusicPlayer(){
    	stopMusicPlayer(true);
    }
    
    public static void stopMusicPlayer(boolean pause){
    	if(musicPlayer!=null){
    		try{
	    		if(musicPlayer.isPlaying()){
	    			if(pause){
	    				musicPlayer.pause();
	    			} else {
	    				musicPlayer.stop();
	    				musicPlayer.release();
	    				isMusicPlaying = false;
	    			}
	    		}
    		} catch(Exception e){
    			musicPlayer = null;
    			isMusicPlaying = false;
    		}
    	}
    }
    
    /**
     * called by the Firearm object when the round count of the weapon changes.
     */
    public static void setRoundCount(Integer count){
    	mHandler.post(new ParamRunnable(count) {
			public void run() {
				roundCount.setText(Integer.toString((Integer)getParam()));
			}
		});
    }
    public static void setRoundCount(String count){
    	mHandler.post(new ParamRunnable(count) {
			public void run() {
				roundCount.setText((String)getParam());
			}
		});
    }
    
    protected final static OnCompletionListener mpComplete = new OnCompletionListener() {
		
		public void onCompletion(MediaPlayer mp) {
			mp.release();
		}
	};
	
	protected final static OnErrorListener mpError = new OnErrorListener() {
		
		public boolean onError(MediaPlayer mp, int what, int extra) {
			mp.release();
			return false;
		}
	};
    
    protected Dialog onCreateDialog(int id) {
		return menuController.prepareDialog(id);
    }
    
    
    protected void onPrepareDialog(int id, Dialog dialog) {
    	
    	dialog = menuController.prepareDialog(id);
    	
    	if(DBG_MODE)
			Log.i("onPrepareDialog","starting...");
    	
    	return;
    }
    
    public static void removeAllDialogs(){
    	((Activity)ctx).removeDialog(Menus.MENU_MAIN);
    	((Activity)ctx).removeDialog(Menus.MENU_OPTIONS);
    	((Activity)ctx).removeDialog(Menus.MENU_TARGET);
    	((Activity)ctx).removeDialog(Menus.MENU_TARGETSELECT);
    }
    
   /* 
    public boolean onCreateOptionsMenu(Menu menu) {
    	//showDialog(Menus.MENU_MAIN);
    	return super.onCreateOptionsMenu(menu);
    }*/
    
    public boolean onPrepareOptionsMenu(Menu menu) {
    	showDialog(Menus.MENU_MAIN);
    	return super.onPrepareOptionsMenu(menu);
    }
    
    
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	if (resultCode == RESULT_OK) {
            if (requestCode == ACTIVITY_RESULT_SELECTPHOTO) {
                Uri selectedImageUri = data.getData();
                
                if(selectedImageUri==null)
                	return;
                
                String selectedImagePath = getPath(selectedImageUri);
                if(DBG_MODE)
                	Log.i("activityResult","user selected photo with uri: "+selectedImagePath);
                Bitmap tmpImage = null;
                
                try{
                	tmpImage =  new BitmapDrawable(ctx.getResources(),selectedImagePath).getBitmap();
                } catch(Exception e){
                	return;
                }
                
                if(GameController.targetMode==AndroidGunz.TARGET_MODE_DEFAULT)
                {
                	gameView.unsetCustomTarget();
                    
                    if(importedImage!=null){
                    	importedImage.recycle();
                    	importedImage = null;
                    }
                    
                    //TODO: revert to CustomTarget class when re-implementing 
                    //	different target sizes/distances.
                    importedImage = Bitmap.createScaledBitmap(tmpImage, 
                    							CustomTarget.WIDTH_LEVEL_1.x, 
                    							CustomTarget.WIDTH_LEVEL_1.y, true);
                	
                	gameView.setCustomTarget(importedImage);
                	int faceCount = gameView.detectCurrentFaces();
                	Toast.makeText(this, 
                					getString(R.string.t_message_import_finished)+" "+faceCount, Toast.LENGTH_LONG).show();
                	gameView.gunThread.refreshTarget();
	                removeAllDialogs();
                }
                else //if(FirearmView.targetMode==AndroidGunz.TARGET_MODE_ANIMATED)
                {
                	gameView.gunThread.hostageView2 = null;
                	gameView.gunThread.targetList[1].sprite = null;
                	gameView.gunThread.targetList[1].sprite = Bitmap.createScaledBitmap(tmpImage, 158, 190, false);
                	removeAllDialogs();
                	Toast.makeText(this,"Custom Target Set!",Toast.LENGTH_SHORT).show();
                }
                
               /* if(tmpImage!=null)
                {
                	tmpImage.recycle();
                	tmpImage = null;
                }*/
                
                //Toast.makeText(ctx, ctx.getString(R.string.t_message_import_finished)+" "+mainTarget.getFaceCount(), Toast.LENGTH_LONG).show();
            }
            else if(resultCode == ACTIVITY_RESULT_OOM) {
            	//showDialog(Menus.MENU_OOM);
            }
        }

    }
    
    protected String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


	public static Context getCtx() {
		return ctx;
	}
	
	protected static void onButtonQuit(){
		((Activity)ctx).finish();
	}
	
	protected static void onButtonTargetmenu(){
		((Activity)ctx).showDialog(Menus.MENU_TARGET);
	}
	
	protected static void onButtonTargetimport(){
		removeAllDialogs();
		Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        ((Activity)ctx).startActivityForResult(Intent.createChooser(intent,
                ctx.getString(R.string.t_selectphoto)), ACTIVITY_RESULT_SELECTPHOTO);
	}
	
	protected void onButtonTargetSet(int resId){
		if(GameController.targetMode==AndroidGunz.TARGET_MODE_DEFAULT){
			gameView.gunThread.changeTargetResource(resId);
			gameView.unsetCustomTarget();
		}
		removeAllDialogs();
	}
	
	public static void onPreloadFinished() {
		hideGameLoadingScreen();
		mHandler.post(new Runnable() {
			public void run() {
				if(LITE_MODE && !adWhirlOn)
				{
					((AndroidGunz)ctx).startAdWhirl();
					adWhirlOn = true;
				}
			}
		});
		//target0.bringToFront();
		//if(DBG_MODE)
		//	showAd();
	}
	
	private static void onOptionChanged(boolean enable){
		if(enable){
			Toast.makeText(AndroidGunz.getCtx(), R.string.t_message_option_enabled, Toast.LENGTH_SHORT).show();
		}
		else
			Toast.makeText(AndroidGunz.getCtx(), R.string.t_message_option_disabled, Toast.LENGTH_SHORT).show();
	}
	
	public void onOptionChangedJohnWoo(boolean enable) {

		MenuActivity.getOptions(ctx).setSwitch(Options.PREF_KEY_OPTION_JOHNWOO, enable);
		
		if(enable){
			//set round count to infinity symbol
			AndroidGunz.setRoundCount(ctx.getString(R.string.symbol_infinity));
		} else {
			AndroidGunz.setRoundCount(gameView.getRoundCount());
		}
		
		//update current firearm config
		gameView.setJohnWooMode(enable);
		onOptionChanged(enable);
	}
	
	public void onOptionChangedAutoReload(boolean enable) {
		
		MenuActivity.getOptions(ctx).setSwitch(Options.PREF_KEY_OPTION_AUTORELOAD, enable);
		
		gameView.setAutoReload(enable);
		
		onOptionChanged(enable);
	}
	
	public void onOptionChangedFacedetect(boolean enable) {
		MenuActivity.getOptions(ctx).setSwitch(Options.PREF_KEY_OPTION_FACEDETECT, enable);
		gameView.setEnableFaceDetection(enable);
		onOptionChanged(enable);
	}
	
	public void onOptionChangedHitbox(boolean enable) {
		MenuActivity.getOptions(ctx).setSwitch(Options.PREF_KEY_OPTION_HITBOXES, enable);
		gameView.setEnableHitboxes(enable);
		onOptionChanged(enable);
	}
	
	public void onOptionChangedHeadshot(boolean enable) {
		MenuActivity.getOptions(ctx).setSwitch(Options.PREF_KEY_OPTION_HEADSHOTS, enable);
		gameView.setEnableHeadshots(enable);
		onOptionChanged(enable);
	}

	public static void onOptionChangedMusicRepeat(boolean enable) {
		
		MenuActivity.getOptions(ctx).setSwitch(Options.PREF_KEY_OPTION_MUSIC_REPEAT, enable);
		
		if(musicPlayer != null){
			stopMusicPlayer();
			startMusicPlayer();
		}
		
		onOptionChanged(enable);
	}
	
	/*private static Runnable loadingScreenImpact = new Runnable() {
		
		public void run() {
			
			if(!loadingScreenOn)
				return;
			
			//determine a random impact image
			long imgIdx = Math.round(Math.random()*3);
			String ident = "glasshole"+imgIdx;
			int resId = ctx.getResources().getIdentifier(ident, "drawable", ctx.getPackageName());
			
			final Drawable d = AndroidGunz.ctx.getResources().getDrawable(R.drawable.glasshole1);
			ImageView iv = new ImageView(AndroidGunz.ctx);
			
			//set a random size scale between 50 and 100%
			double sizeScale = Math.random();
			//if(sizeScale<0.5)
				sizeScale = 0.5;
			
			int sizeX = d.getIntrinsicWidth()/2;
			int sizeY = d.getIntrinsicHeight()/2;
			
			sizeX = (int) Math.floor(sizeX*sizeScale);
			sizeY = (int) Math.floor(sizeY*sizeScale);
			
			RelativeLayout.LayoutParams rl = new RelativeLayout.LayoutParams(sizeX, sizeY);
			
			long xOffset = Math.round(Math.random()*loadingScreen.getWidth());
			if(xOffset==loadingScreen.getWidth())
				xOffset = loadingScreen.getWidth()-d.getIntrinsicWidth();
			
			long yOffset = Math.round(Math.random()*loadingScreen.getHeight());
			if(yOffset==loadingScreen.getHeight())
				yOffset = loadingScreen.getHeight()-d.getIntrinsicHeight();
			
			rl.leftMargin = (int) xOffset;
			rl.topMargin = (int) yOffset;
			
			iv.setLayoutParams(rl);
			iv.setBackgroundDrawable(d);

			mHandler.post(new ParamRunnable(iv) {
				
				public void run() {
					new Thread(new Runnable() {
						
						public void run() {
							//MediaPlayer mp = MediaPlayer.create(AndroidGunz.ctx, R.raw.bottlebreak);
							//mp.setOnCompletionListener(mpComplete);
							//mp.start();
						}
					}).start();
					loadingScreen.addView((ImageView)this.getParam());
					((TextView)loadingScreen.findViewById(R.id.label_loading)).bringToFront();
				}
			});
		}
	};*/
	
	/*private static Runnable loadingScreenControllerRunnable = new Runnable() {
		
		public void run() {
			double maxTime = 10000;
			int maxInterval = 800;
			int minInterval = 200;
			double currentTime = 0;
			double x=0;
			
			for(;x<=maxTime;){
				
				if(Thread.currentThread().isInterrupted())
					break;
				
				if(!AndroidGunz.loadingScreenOn)
					break;
				
				double tmpInterval = minInterval + (maxInterval * Math.random());
				currentTime = currentTime+tmpInterval;
				
				mHandler.postDelayed(loadingScreenImpact, Math.round(currentTime));
				
				x = x+tmpInterval;
			}
			return;
		}
	};*/
	
	private static Runnable rShowLoadingScreen = new Runnable() {
		
		public void run() {
			//loadingScreen.buildDrawingCache();
			
			Animation a = new AlphaAnimation(1.0f, 0.0f);
			a.setRepeatCount(Animation.INFINITE);
			//a.setFillAfter(true);
			a.setRepeatMode(Animation.REVERSE);
			a.setDuration(750);
			
			loadingScreen.setBackgroundResource(R.drawable.loading_bg);
			loadingScreen.setVisibility(View.VISIBLE);
			loadingScreen.bringToFront();
			
			TextView tv = (TextView)loadingScreen.findViewById(R.id.label_loading);
			
			tv.setTextColor(Color.DKGRAY);
			tv.setTypeface(arFont);
			tv.startAnimation(a);
			
			//loadingScreen.invalidate();
			
			/*if(loadingScreenController!=null && loadingScreenController.isAlive()){
				loadingScreenController.interrupt();
			}*/
		}
	};
	
	protected void showGameLoadingScreen()
	{
		if(loadingScreenOn==false)
		{
			loadingScreenOn = true;
			
			//mHandler.removeCallbacks(rHideLoadingScreen);
			//mHandler.removeCallbacks(rShowLoadingScreen);
			mHandler.postAtFrontOfQueue(rShowLoadingScreen);
		}
		
	}
	
	private static Runnable rHideLoadingScreen = new Runnable() {
		
		public void run() {
			//Animation a = AnimationUtils.loadAnimation(AndroidGunz.ctx, android.R.anim.fade_out);
			TextView tv = (TextView)loadingScreen.findViewById(R.id.label_loading);
			tv.clearAnimation();
			tv.setVisibility(View.GONE);
			//tv.destroyDrawingCache();
			//loadingScreen.setVisibility(View.INVISIBLE);
			loadingScreen.setBackgroundResource(0);
			loadingScreen.setVisibility(View.INVISIBLE);

			roundView.bringToFront();
			scoreView.bringToFront();
			bMenu.bringToFront();
			
			//if(LITE_MODE && adWhirlLayout!=null)
			//	adWhirlLayout.bringToFront();
			//roundCount.bringToFront();
			//loadingScreen.destroyDrawingCache();
			return;
		}
	};
	
	protected static void hideGameLoadingScreen()
	{
		if(loadingScreenOn==true)
		{
			loadingScreenOn = false;
			//mHandler.removeCallbacks(loadingScreenImpact);
			
			//mHandler.removeCallbacks(rShowLoadingScreen);
			//mHandler.removeCallbacks(rHideLoadingScreen);
			//mHandler.postAtFrontOfQueue(rHideLoadingScreen);
			mHandler.post(rHideLoadingScreen);
		}
	}
	
	
	protected void onDestroy() {
		stopMusicPlayer();
		if(loadingScreenOn){
			hideGameLoadingScreen();
		}
		
		if(isFinishing()){
			currentScore = 0;
			finalScore = 0;
			roundCount = null;
			roundView = null;
			loadingScreen = null;
			gameView.destroy();
			gameView = null;
		}
		
		super.onDestroy();
	}
	
	
	protected void onPause() {
		pauseMusicPlayer();
		if(loadingScreenOn){
			hideGameLoadingScreen();
		}
		super.onPause();
	}
	
	
	protected void onResume() {
		super.onResume();
		
		if(isMusicPlaying)
		{
			if(!resumeMusicPlayer()){
				startMusicPlayer();
			}
		}
		//gunview.drawIdle();
	}
	
	
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}
	
	
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		//gameView = (GameController) guncon.findViewById(791);
		//showGameLoadingScreen();
		super.onRestoreInstanceState(savedInstanceState);
	}
	
	private void setVibrator(Vibrator vibrator) {
		this.vibrator = vibrator;
	}

	public Vibrator getVibrator() {
		return vibrator;
	}

	public static void onOptionChangedMusicTrack(String musicTrack) {
		MenuActivity.getOptions(ctx).setString(Options.PREF_KEY_OPTION_MUSIC, 
											musicTrack);
		if(musicTrack.equals(Options.MUSIC_TRACK_NONE)){
			stopMusicPlayer();
		} else {
			startMusicPlayer();
		}
	}
	
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if(keyCode == KeyEvent.KEYCODE_BACK){
			finish();
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}

	/*private static AdSenseSpec updateAdsenseSpec()
	{
		AdSenseSpec adSenseSpec =
            new AdSenseSpec(ADSENSE_ID)     // Specify client ID. (Required)
            .setCompanyName(ctx.getString(R.string.adsense_co_name))  // Set company name. (Required)
            .setAdFormat(AdFormat.FORMAT_300x250)
            .setAppName(ctx.getString(R.string.app_name))          // Set application name. (Required)
            .setKeywords(ctx.getString(R.string.adsense_keywords)+","+ctx.getString(R.string.app_name))         // Specify keywords.
            .setChannel(ctx.getString(R.string.adsense_channels))        // Set channel ID.
            .setColorBorder("6699CC,000000,CCCCCC")
            .setColorBackground("003366,000000,CCCCCC")
            .setColorLink("FFFFFF,FFFFFF,000000")
            .setColorText("AECCEB,CCCCCC,333333")
            .setColorUrl("AECCEB,999999,666666")
            .setAdType(AdType.TEXT_IMAGE)        // Set ad type to Text.
            .setAdTestEnabled(AndroidGunz.DBG_MODE) // Keep true while testing.
            .setExpandDirection(ExpandDirection.BOTTOM);  
		
		return(adSenseSpec);
	}

	public static void onNagThreshold() {
		if(LITE_MODE)
			showAd();
	}*/

	public static void onOptionNotPurchased() {
		mHandler.post(new Runnable() {
			public void run() {
				Toast.makeText(ctx, R.string.t_message_upgrade, Toast.LENGTH_LONG).show();
			}
		});
	}
	
	public static void kill()
	{
		((AndroidGunz)ctx).finish();
	}

	public void startAdWhirl()
	{
		AdWhirlManager.setConfigExpireTimeout(1000 * 60 * 5);
		//AdWhirlLayout adWhirlLayout = new AdWhirlLayout(this,ADWHIRL_SDK_KEY);
		//AdWhirlLayout adWhirlLayout = (AdWhirlLayout)findViewById(R.id.ad_con);
		AdWhirlTargeting.setTestMode(AndroidGunz.DBG_MODE);
		
		int diWidth = 320;
		int diHeight = 52;
		
		float density = getResources().getDisplayMetrics().density;
		adWhirlLayout.setMaxWidth((int)(diWidth * density));
		adWhirlLayout.setMaxHeight((int)(diHeight * density));

		
		String keywords[] = getResources().getStringArray(R.array.ad_keywords);
		HashSet<String> keywordHash = new HashSet<String>(Arrays.asList(keywords));
		
		//keywordHash.add(getString(R.string.app_name));
		
		AdWhirlTargeting.setKeywordSet(keywordHash);
		AdWhirlAdapter.setGoogleAdSenseAppName("GunZ");
		AdWhirlAdapter.setGoogleAdSenseCompanyName(getString(R.string.adsense_co_name));
		AdWhirlAdapter.setGoogleAdSenseChannel(ctx.getString(R.string.adsense_channels));
		AdWhirlAdapter.setGoogleAdSenseExpandDirection("BOTTOM");
		//RelativeLayout rl = (RelativeLayout)findViewById(R.id.ad_con);
		//rl.setGravity(Gravity.CENTER_HORIZONTAL);
		//rl.setVisibility(View.VISIBLE);
		//rl.bringToFront();
		//rl.addView(adWhirlLayout,layoutParams);
		//rl.invalidate();
		//adWhirlLayout.setVisibility(View.VISIBLE);
		//adWhirlLayout.bringToFront();
		//adWhirlLayout.invalidate();
	}
	
	public void adWhirlGeneric() {
		Log.w("AdWhirl","adWhirlGeneric called!");
		if(LITE_MODE)
			adWhirlLayout.bringToFront();
		else
			adWhirlLayout.setVisibility(View.INVISIBLE);
	}
	
	public static void log(String s)
    {
    	if(s==null)
    		return;
    	Log.w("GunZ DLC",s);
    }

}
