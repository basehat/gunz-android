package com.swn.gunz.meulite;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;

public class Firearm extends SpriteController {
	
	protected static final boolean DBG_MODE = true;
	
	protected int gunId;
	protected static int gunDischargeAnimsCount = 1;
	protected FirearmConfig config = null;
	
	protected static final float ANIMATION_DELAY = 25.0f;
	
	protected static final int FLASH_LENGTH = 2;
	
	protected static final String ANIM_STAGE_SHOOT = "shoot";
	protected static final String ANIM_STAGE_SHOOTLAST = "shootlast";
	protected static final String ANIM_STAGE_DRAW = "draw";
	protected static final String ANIM_STAGE_RELOAD = "reload";
	protected static final String ANIM_SOUND_SLIDEBACK = "slideback";
	
	public static final int WEAPON_STATE_IDLE = 0;
	public static final int WEAPON_STATE_FIRE = 1;
	public static final int WEAPON_STATE_FIRELAST = 2;
	public static final int WEAPON_STATE_RELOAD = 3;
	public static final int WEAPON_STATE_SLIDEBACK = 4;
	
	public static final int DEFAULT_DISCHARGE_FRAME = 1;
	public static final int DEFAULT_RELOAD_FRAME = 1;
	
	protected static int currentAnimationState = WEAPON_STATE_IDLE;
	private static int currentAnimationFrame = 0;
	private static int currentFlashFrame = 0;
	private static int flashFrameCount = 0;
	
	public int roundsLeft = 0;
	
	public Bitmap fireAnimation[];
	public Bitmap flashAnimation[];
	public Point fireAnimationOffset;
	public Bitmap reloadAnimation[];
	public Point reloadAnimationOffset;
	public Point flashOffset;
	public PointF flashRotatePivot;
	
	private Paint mPaint;
	
	private boolean isReady = false;
	public String gunPath = null;
	
	//private Random randObj = new Random();
	//private float nextRand = 0.f;
	private float rotateRand1 = 90.f;
	
	public Firearm(Context c, FirearmConfig fc) {
		super(c);
		
		mPaint = new Paint();
		mPaint.setAntiAlias(false);
		mPaint.setDither(true);
		
		config = fc;
		//nextRand = (float)(Math.random()*360);//randObj.nextFloat();
	}
	
	@Override
	public void render(Canvas c)
	{
		if(!isReady)
			return;
		
		long now = System.currentTimeMillis();
    	double elapsed = (now - mLastTime);
    	mLastTimeFrame += elapsed;
    	elapsed = elapsed/1000.0;
    	
    	switch(currentAnimationState)
    	{
    		case(WEAPON_STATE_IDLE):
    			c.drawBitmap(fireAnimation[0], fireAnimationOffset.x, fireAnimationOffset.y, mPaint);
    			break;
    		case(WEAPON_STATE_RELOAD):
    			mLastTimeFrame += elapsed;
			
				if(mLastTimeFrame>ANIMATION_DELAY)
				{
					currentAnimationFrame++;
					mLastTimeFrame = 0;
				}
				
				if(currentAnimationFrame>=reloadAnimation.length)
    			{
    				currentAnimationState = WEAPON_STATE_IDLE;
    				roundsLeft = config.magazineCap;
    				currentAnimationFrame = 0;
    				c.drawBitmap(fireAnimation[0], fireAnimationOffset.x, fireAnimationOffset.y, mPaint);
    				GameController.updateRoundCount(roundsLeft);
    				
    				break;
    			} else if(currentAnimationFrame==DEFAULT_RELOAD_FRAME){
    				GameController.onReload();
    				currentAnimationFrame++;
    			}
				
				c.drawBitmap(reloadAnimation[currentAnimationFrame], reloadAnimationOffset.x, reloadAnimationOffset.y, mPaint);
    			break;
    		case(WEAPON_STATE_FIRE):
    			
    			mLastTimeFrame += elapsed;
    			
    			if(mLastTimeFrame>ANIMATION_DELAY)
    			{
    				currentAnimationFrame++;
    				mLastTimeFrame = 0;
    			}
    			
    			if(currentAnimationFrame>=fireAnimation.length)
    			{
    				currentAnimationState = WEAPON_STATE_IDLE;
    				c.drawBitmap(fireAnimation[0], fireAnimationOffset.x, fireAnimationOffset.y, mPaint);
    				break;
    			}
    			else if(currentAnimationFrame==DEFAULT_DISCHARGE_FRAME)
    			{
    				GameController.onDischarge(roundsLeft);
    				currentAnimationFrame++;
    				flashFrameCount = 0;
    			}
    			
    			if(flashFrameCount<=FLASH_LENGTH)
				{
					if(currentFlashFrame==flashAnimation.length){
						currentFlashFrame = 0;
					}
					
					c.save();
					c.rotate(rotateRand1, flashRotatePivot.x,
										  flashRotatePivot.y);
					c.drawBitmap(flashAnimation[currentFlashFrame], flashOffset.x, flashOffset.y, null);
					c.restore();
					
					currentFlashFrame++;
					flashFrameCount++;
				}
    			
    			c.drawBitmap(fireAnimation[currentAnimationFrame], fireAnimationOffset.x, fireAnimationOffset.y, mPaint);
    			break;
    	}
    	
    	mLastTime = System.currentTimeMillis();
	}
	
	public boolean fire()
	{
		if(currentAnimationState==WEAPON_STATE_RELOAD)
			return false;
		
		if(roundsLeft>0){
			flashFrameCount = 99;
			currentFlashFrame = 0;
			startAnimation(WEAPON_STATE_FIRE);
			roundsLeft--;
			return true;
		} else {
			//nextRand = (float)(Math.random()*360);
			rotateRand1 = (float)(Math.random()*360);
			startAnimation(WEAPON_STATE_RELOAD);
		}
		return false;
	}
	
	private void startAnimation(int state)
	{
		currentAnimationState = state;
		mLastTimeFrame = 0;
		currentAnimationFrame = 0;//refire while firing, COD style
	}
	
	public static boolean extractPackage(String gundir, boolean refresh)
	{
		try {
			
			String cacheDir = gundir+Util.GUNZ_GUN_DIRECTORY_CACHE;
			File fcacheDir = new File(cacheDir);
			
			if(!fcacheDir.isDirectory()){
				fcacheDir.mkdirs();
			}
			
			if((new File(cacheDir+FireArms.GUNZ_ZPACKAGE_FILENAME_CONFIG).isFile())){
				if(refresh){
					//clean out existing cache
					Util.deleteDir(fcacheDir);
					fcacheDir.mkdirs();
				} else {
					//package already seems to still be extracted from last go
					return true;
				}
			}
			
			ZipFile z = new ZipFile(gundir+FireArms.GUNZ_GUN_FILENAME_ZPACKAGE);
			
			Enumeration<? extends ZipEntry> entries = z.entries();
			
			while(entries.hasMoreElements()){
				
				ZipEntry entry = (ZipEntry)entries.nextElement();

				String dest = cacheDir+entry.getName().replace(FireArms.GUNZ_ZPACKAGE_ROOT, "");
				
				if(entry.isDirectory()){
					if(AndroidGunz.DBG_MODE)
						AndroidGunz.log("unpacking directory: "+entry.getName());
					if(!entry.getName().equals(FireArms.GUNZ_ZPACKAGE_ROOT))
						(new File(dest)).mkdir();
					continue;
				}
				
				if(AndroidGunz.DBG_MODE)
					AndroidGunz.log("unpacking file: "+entry.getName());
				
				Util.copyInputStream(z.getInputStream(entry),
						new BufferedOutputStream(new FileOutputStream(dest)));
			}
			
			z.close();
			
			return true;
			
		} catch (Exception e) {
			if(AndroidGunz.DBG_MODE)
				e.printStackTrace();
		}
		return false;
	}
	
	public boolean loadPackage(int gcWidth, int gcHeight)
	{
		isReady = false;
		
		fireAnimation = Util.getAnimation(ctx,ANIM_STAGE_SHOOT,1);
		fireAnimationOffset = new Point(gcWidth-fireAnimation[0].getWidth(),
				gcHeight-fireAnimation[0].getHeight());
		
		reloadAnimation = Util.getAnimation(ctx,ANIM_STAGE_RELOAD,1);
		reloadAnimationOffset = new Point(gcWidth-reloadAnimation[0].getWidth(),
				gcHeight-reloadAnimation[0].getHeight());
		
		if(config.hasMuzzleFlash){
			flashAnimation = Util.getDefaultMuzzleFlashBitmap(ctx);
		}
		
		roundsLeft = config.magazineCap;
		flashOffset = new Point();
		flashOffset.x = (int) Util.pixelsToDip(ctx,config.muzzleFlashOffsetX);
		flashOffset.y = (int) Util.pixelsToDip(ctx,config.muzzleFlashOffsetY+fireAnimationOffset.y);
		
		flashRotatePivot = new PointF(flashOffset.x+flashAnimation[0].getWidth()/2,
										flashOffset.y+flashAnimation[0].getHeight()/2);
		
		GameController.updateRoundCount(roundsLeft);
		
		isReady = true;
		return true;
	}
	
	public boolean preLoad()
	{
		if(gunPath==null || gunPath.length()==0){
			return false;
		}
		
		isReady = false;
		
		
		
		
		return false;
	}
	
	/*public void setReady(boolean ready)
	{
		isReady = ready;
	}*/

	public boolean isReady() {
		return isReady;
	}

	@Override
	void onDestructionCycle(Canvas c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	void onThrowCycle(Canvas c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCoinSize(int size) {
		// TODO Auto-generated method stub
		
	}
}
