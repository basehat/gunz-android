package com.swn.gunz.meulite;

public class FirearmBio {
	protected String usedBy;
	protected String usedWars;
	protected String designer;
	protected String designedYear;
	protected String weight;
	protected String length;
	protected String lengthBarrel;
	protected String cartdrigeVelocity;
	protected String cartridgePower;
	protected String cartdrigeName;
	protected String cartridgeRange;
	protected String actionType;
	protected String fireRate;
	protected String description;
	protected String history;
	protected String feed;
	
	public void destroy(){
		this.history = null;
		this.description = null;
	}
}
