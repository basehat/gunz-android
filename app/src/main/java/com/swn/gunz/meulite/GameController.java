package com.swn.gunz.meulite;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.media.AudioManager;
import android.media.FaceDetector;
import android.media.SoundPool;
import android.media.FaceDetector.Face;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

class GameController extends SurfaceView implements SurfaceHolder.Callback
{
	public static final String WEAPON_MEU_1911 = "meu";
	
	public static final int DEFAULT_SPEED_MODIFIER = 100;
	public static final int DEFAULT_DELAY_MODIFIER = 200;
	
	public static int targetMode = AndroidGunz.TARGET_MODE_DEFAULT;
	
	//color used for greenscreen
	protected static final int[] GS_COLOR = new int[]{0, 255, 0};
	private static final int MAX_MEDIAPLAYERS = 4;
	public FirearmThread gunThread;
	public boolean surfaceReady = false;
	private String currentAnimationName = null;
	
	public static Runnable rAnimationFinished = null;
	
	protected boolean preloading = false;
	
	private PointF eyesMidPts[] = new PointF[Target.FD_MAX_FACES];
	private float  eyesDistance[] = new float[Target.FD_MAX_FACES];
	protected PointF circlePts[] = new PointF[1];
	protected float headshotRadius;
	
	private static int soundReloadId;
	private static int soundFireId;
	private static int soundScoreId;
	private static int soundMiscId;
	private static int soundHeadshotId;
	
	private static Vibrator vibrator;
	
	private boolean enableHeadshots = false;
	private boolean enableFaceDetection = false;
	private boolean enableHitboxes = false;
	
	private static Timer t;
	
	protected Firearm weapon;
	protected static FirearmConfig config = null;
	
	private static final int GUN_VIBRATE_TIME = 150;
	private static final int GUN_VIBRATE_TIME_RELOAD = 100;
	
	private static SoundPool soundPool;
	
	private Context ctx;

	private Bitmap levelBackground;
	
	class MovingTarget {
		public int pointsValue;
		public PointF startLocation;
		public PointF currentLocation;
		public PointF endLocation;
		public int material; 
		public int value;
		//public Bitmap idleAnimation ;
		public boolean moving;
		public boolean finished;
		public boolean movingRight = false;
		public int speed = 75; //75 is about an average speed
		public int currentSpeed = 75;
		public Bitmap sprite;
		public boolean friendly;
		public boolean down = false;
		public int delayFrames = 0;
	}
	
	class PointMarker {
		public PointF location;
		public int value;
		public int color = Color.RED;
		public int size = 40;
		public String message;
		public int alpha = 200;
		public int fallX = 0;
		public int fallY = 0;
		public double fallDirectionX = 0;
		public double fallDirectionY = 0;
	}
	
	public GameController(Context context, AttributeSet attrs) {
		super(context, attrs);
		SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        ctx = context;
        t = new Timer();
        
        config = Util.parseConfig(ctx);
        
        soundPool = new SoundPool(MAX_MEDIAPLAYERS,
				AudioManager.STREAM_MUSIC,
				0);
        vibrator = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
        
        weapon = new Firearm(ctx, config);
        
        gunThread = new FirearmThread(holder, context, null);
        //setFocusable(true);
	}
	
	public void fireWeapon(PointF impact)
	{
		//CoinShot.log("got motion event at: "+impact.x+","+impact.y);
		gunThread.impactOffset = impact;
		if(weapon.fire())
		{
			setGunDischarge(true);
			//gunThread.impactOffset = impact;
		}
	}
	
	 public int getRoundCount()
     {
     	return weapon.roundsLeft;
     }
	
	public static void setTargetMode(int mode)
	{
		targetMode = mode;
	}
	
	 public void onHeadshot(){
	    	if(!isEnableFaceDetection() ||  !isEnableHeadshots()){
	    		return;
	    	}
	    	
	   	soundPool.play(soundHeadshotId, 1.0f, 1.0f, 1, 0, 1.0f);
	}
	
	public void refreshTarget()
	{
		gunThread.refreshTarget();
	}
	 
	class FirearmThread extends Thread
	{
		private Paint mPaint;
        private Paint tPaint;
        
        /** Handle to the surface manager object we interact with */
        public SurfaceHolder mSurfaceHolder;
        public boolean isSurfaceReady = false;
        public boolean animationStopped = true;
        public int marginTop = 0;
        public int marginLeft = 0;
        public PointF idleMargin = new PointF();
        public int gsColor;
        public PointF bgOffset;
        public PointF targetOffset;
        private Paint pFlash;
        private Paint pHeadshot;
        public boolean gunDischarge = true;
        public PointF flashOffset;
        //public boolean drawIdle = false;
        public boolean mRun = false;
        public boolean started = false;
        private long mLastTime;
        private int bulletIdx = 0;
        private PointF targetHolderOffset;
        
        public Bitmap[] currentAnimation = null;
        public Bitmap[] flashAnimation = null;
        public PointF[] bulletHoles = null;
        
        public Bitmap bgImage;
        public Bitmap hostageView;
        public Bitmap hostageView2;
        public Bitmap hostageView3;
        public Bitmap hostageView4;
        public Bitmap targetDown;
        public Bitmap targetView;
        public Bitmap customTarget = null;
        public Bitmap bulletView;
        public Bitmap targetHolderView;
        
        private float screenWidth;
        
        
        public PointF impactOffset = null;
        
        public ArrayList<PointMarker>  markerStack;
        public ArrayList<MovingTarget> targetStack;
        
        public MovingTarget targetList[];
        
        public class myFilter extends ColorFilter{
        	
        }
        
        private void renderTargets(Canvas c){
        	
        	//update positions
        	updateTargets();
        	
        	long now = System.currentTimeMillis();
        	double elapsed = (now - mLastTime) / 1000.0;
        	//if (mLastTime > now) return;
        	
        	for(int x=0;x<targetStack.size();x++){
        		MovingTarget tmpTarget = targetStack.get(x);
        		if(tmpTarget.moving)
                {
        			double random1 = Math.random();
        			double random2 = Math.random();
        			
                	if(tmpTarget.movingRight){
                		//testTarget.currentLocation.x += (200*elapsed);
                		double inc = elapsed * tmpTarget.currentSpeed;
                		tmpTarget.currentLocation.x += inc;
                		
                		if(tmpTarget.currentLocation.x >= tmpTarget.endLocation.x){
                			//testTarget.moving = false;
                			tmpTarget.movingRight = false;
                			
                			//change speed
                			tmpTarget.currentSpeed = (int)((random1*DEFAULT_SPEED_MODIFIER)+tmpTarget.speed);
                			
                			//tmpTarget.startLocation.x = (float)((Math.random()*(tmpTarget.currentLocation.x+75)));
                			//tmpTarget.startLocation.x = (float) (Math.random()*(screenWidth-tmpTarget.sprite.getWidth()));//-tmpTarget.sprite.getWidth();
                			tmpTarget.startLocation.x = (float) (random2*((screenWidth/2)+150)) - 150; 	
                			//reset targets when they change direction
                			tmpTarget.down = false;
                			tmpTarget.currentLocation.y = (tmpTarget.startLocation.y);
                			
                			if(tmpTarget.currentLocation.x>=screenWidth)
                			{
	                			tmpTarget.moving = false;
	                			tmpTarget.delayFrames = (int)(random1*DEFAULT_DELAY_MODIFIER);
	                			
	                			if(tmpTarget.delayFrames>0)
	                			{
		                			if(Math.random()>=0.9){
		                				//swap sides randomly
		                				tmpTarget.movingRight = true;
		                				tmpTarget.startLocation.x = (float) tmpTarget.sprite.getWidth()*-1;
		                				tmpTarget.currentLocation.x = (float) tmpTarget.sprite.getWidth()*-1;
		                				tmpTarget.endLocation.x = (float)((random2*screenWidth)+150);
		                			}
	                			}
                			}
                		}
                			
                	} else {
                		double inc = elapsed * tmpTarget.currentSpeed;
                		tmpTarget.currentLocation.x -= inc;
                		
                		if(tmpTarget.currentLocation.x <= tmpTarget.startLocation.x){
                			//testTarget.moving = false;
                			tmpTarget.movingRight = true;
                			
                			tmpTarget.currentSpeed = (int)((random1*DEFAULT_SPEED_MODIFIER)+tmpTarget.speed);
                			
                			tmpTarget.endLocation.x = (float)((random2*screenWidth)+150);
                			
                			//reset targets when they change direction
                			tmpTarget.down = false;
                			tmpTarget.currentLocation.y = (tmpTarget.startLocation.y);
                			
                			//introduce a possible delay while we are offscreen
                			if(tmpTarget.currentLocation.x<=(tmpTarget.sprite.getWidth()*-1))
                			{
	                			tmpTarget.moving = false;
	                			tmpTarget.delayFrames = (int)(random1*DEFAULT_DELAY_MODIFIER);
                			}
                		}
                	}
                	
                	//gun was fired, lets check if our target was hit
                	if(gunDischarge && !tmpTarget.down && impactOffset!=null)
                	{
                		if(impactOffset.x > tmpTarget.currentLocation.x){
                			if(impactOffset.y > tmpTarget.currentLocation.y){
    	            			if(impactOffset.x < tmpTarget.currentLocation.x+tmpTarget.sprite.getWidth()){
    	            				if(impactOffset.y < tmpTarget.currentLocation.y+tmpTarget.sprite.getHeight()){
    	            				
	    	            				int spriteOffsetX = (int) (impactOffset.x - tmpTarget.currentLocation.x);
	    	            				int spriteOffsetY = (int) (impactOffset.y - tmpTarget.currentLocation.y);
	    	            				
	    	            				if(tmpTarget.sprite.getPixel(spriteOffsetX, spriteOffsetY) !=
	    	            						Color.TRANSPARENT){
	    	            					//impact occured on target
	        	            				//Log.i("FirearmView","Impact on moving target");
	        	            				
	    	            					tmpTarget.down = true;
	    	            					tmpTarget.currentSpeed = 255;
	    	            					
	    	            					tmpTarget.currentLocation.y = (tmpTarget.currentLocation.y+tmpTarget.sprite.getHeight())-11;
	    	            					
	        	            				PointMarker p = new PointMarker();
	        	            				p.location = new PointF(impactOffset.x,impactOffset.y);
	        	            				
	        	            				if(tmpTarget.friendly){
	        	            					p.color = Color.RED;
	        	            					p.value = tmpTarget.value;
	            	            				p.message = Integer.toString(tmpTarget.value);
	        	            				}
	        	            				else {
	        	            					p.color = Color.GREEN;
	        	            					p.value = tmpTarget.value;
	            	            				p.message = "+"+Integer.toString(tmpTarget.value);
	        	            				}
	        	            				
	        	            				p.fallDirectionX = random1;
	        	            				p.fallDirectionY = random2;
	        	            				
	        	            				markerStack.add(p);
	    	            				}
	    	            				
	    	            				//comment to allow bullet pass-through into other target,
	    	            				//	at the cost of some performance
	    	            				//impactOffset = null;
	    	            				
    	            				}
    	            			}
                			}
                		}
                	}
                }
        		else
        		{
        			//target is not moving,
        			//continue countdown until were moving again
        			if(tmpTarget.delayFrames > 0){
        				tmpTarget.delayFrames--;
        			} else {
        				tmpTarget.moving = true;
        			}
        		}
        		
        		if(!tmpTarget.down)
            	{
                	c.drawBitmap(tmpTarget.sprite, 
                			tmpTarget.currentLocation.x, 
                			tmpTarget.currentLocation.y, null);
            	}
            	else
            	{
            		c.drawBitmap(targetDown, 
                			tmpTarget.currentLocation.x, 
                			tmpTarget.currentLocation.y, null);
            	}
			}
        	
        	impactOffset = null;
        	mLastTime = now;
        }
        
        public void updateTargets()
        {
            if(targetStack.size()==0){
            	
            	hostageView = BitmapFactory.decodeResource(ctx.getResources(),
    					R.drawable.hostage1);
                hostageView2 = BitmapFactory.decodeResource(ctx.getResources(),
    					R.drawable.hostage2);
                
                targetList[0] = new MovingTarget();
                targetList[1] = new MovingTarget();
                targetList[2] = new MovingTarget();
                
                int tmpHeight = getHeight();
                //int tmpWidth = getWidth();
                
            	if(targetMode==AndroidGunz.TARGET_MODE_ANIMATED)
            	{
            		hostageView4 = BitmapFactory.decodeResource(ctx.getResources(),
        					R.drawable.hostage4);
            		
            		int tmpStartY = (int)(((double)tmpHeight*0.35)+Math.random()*0.10);
            		
            		targetList[0].startLocation = new PointF((float)(((Math.random()*100)+100)*-1),
	            											Util.pixelsToDip(ctx,tmpStartY));
	            	
            		targetList[0].endLocation = new PointF((float)(((Math.random()*200)+100)),
	            											Util.pixelsToDip(ctx,tmpStartY));
            		targetList[0].currentLocation = new PointF(targetList[0].startLocation.x,
            				targetList[0].startLocation.y);
            		targetList[0].moving = true;
            		targetList[0].movingRight = true;
            		targetList[0].sprite = hostageView4;
            		targetList[0].friendly = false;
            		targetList[0].speed = 250;
            		targetList[0].value = 1100;
            	}
            	else
            	{
            		hostageView3 = BitmapFactory.decodeResource(ctx.getResources(),
        					R.drawable.hostage3);
            		
            		targetList[0].startLocation = new PointF((float)(((Math.random()*100)+100)*-1),
            				Util.pixelsToDip(ctx,65));

            		targetList[0].endLocation = new PointF((float)(((Math.random()*200)+100)),
            				Util.pixelsToDip(ctx,65));
            		targetList[0].currentLocation = new PointF(targetList[0].startLocation.x,
            				targetList[0].startLocation.y);
            		targetList[0].moving = true;
            		targetList[0].movingRight = true;
            		targetList[0].sprite = hostageView3;
            		targetList[0].friendly = true;
            		targetList[0].speed = 220;
            		targetList[0].value = -750;
            	}
            	
            	targetList[1].startLocation = new PointF((float)(((Math.random()*100)+100)*-1),
            			Util.pixelsToDip(ctx,90));

            	targetList[1].endLocation = new PointF((float)(((Math.random()*200)+100)),
            				Util.pixelsToDip(ctx,90));
            	
            	targetList[1].currentLocation = new PointF(targetList[1].startLocation.x,
            			targetList[1].startLocation.y);
            	targetList[1].moving = true;
            	targetList[1].movingRight = true;
            	targetList[1].sprite = hostageView2;
            	targetList[1].friendly = false;
            	targetList[1].value = 400;
            	
            	int tmpStartY = (int)(((double)tmpHeight*0.20)+Math.random()*0.10);
            	
            	targetList[2].startLocation = new PointF((float)(((Math.random()*100)+100)*-1),
            									Util.pixelsToDip(ctx,tmpStartY));
            	targetList[2].endLocation = new PointF((float)(((Math.random()*200)+200)),
            									Util.pixelsToDip(ctx,tmpStartY));
            	
            	
            	targetList[2].currentLocation = new PointF(targetList[2].startLocation.x,
            			targetList[2].startLocation.y);
            	targetList[2].moving = true;
            	targetList[2].movingRight = false;
            	targetList[2].sprite = hostageView;
            	targetList[2].friendly = false;
            	targetList[2].value = 460;
            	
            	if(targetMode==AndroidGunz.TARGET_MODE_ANIMATED)
            	{
	            	targetStack.add(targetList[0]);
	            	targetStack.add(targetList[2]);
	            	targetStack.add(targetList[1]);
            	}
            	else 
            	{
	            	targetStack.add(targetList[2]);
	            	targetStack.add(targetList[0]);
	            	targetStack.add(targetList[1]);
            	}
            }
        }
        
        public void drawMarkers(Canvas c)
        {
        	for (PointMarker p : markerStack) {
        		
        		if(p.alpha<=0)
        			continue;
        		
        		if(p.alpha==200){
        			AndroidGunz.updateScore(p.value);
        		}
        		
        		tPaint.setColor(p.color);
        		tPaint.setAlpha(p.alpha);
        		tPaint.setTextSize(p.size);
        		tPaint.setTypeface(AndroidGunz.arFont);
        		
        		c.drawText(p.message, p.location.x+p.fallX, p.location.y+p.fallY, tPaint);
        		
        		p.alpha -= (p.fallDirectionX*2)+4;
        		
        		if(p.fallDirectionX>=0.51){
        			p.fallX += 1;
        		} else {
        			p.fallX -= 1;
        		}
        		
        		if(p.fallDirectionY>=0.51){
        			p.fallY += 2;
        		} else {
        			p.fallY -= 2;
        		}
			}
        }
        
        public void drawHoles(Canvas c)
        {
        	for (PointF p : bulletHoles) {
				if(p!=null)
				{
					c.drawBitmap(bulletView,
							p.x,p.y, tPaint);
				}
			}
        }
        
        public void drawTarget(Canvas c)
        {
        	if(customTarget!=null && !customTarget.isRecycled())
        		c.drawBitmap(customTarget,
    					targetOffset.x,targetOffset.y, null);
        	else
        		c.drawBitmap(targetView,
					targetOffset.x,targetOffset.y, null);
        	
        	c.drawBitmap(targetHolderView,
        				targetHolderOffset.x,targetHolderOffset.y, null);
        	
        	drawFaces(c);
        	
        	if(gunDischarge){
        		if(impactOffset!=null)
        		{
	        		if(impactOffset.x > targetOffset.x){
	        			if(impactOffset.x < targetOffset.x+targetView.getWidth()){
	        				if(impactOffset.y > targetOffset.y){
	        					if(impactOffset.y < targetOffset.y+targetView.getHeight()){
	        						//impact on stationary target
	        						bulletHoles[bulletIdx] = new PointF(impactOffset.x,impactOffset.y);
	        						bulletIdx++;
	        						
	        						if(bulletIdx > (Target.MAX_BULLETHOLES-1)){
	        							bulletIdx = 0;
	        						}
	        					}
	        				}
	        			}
	        		}
	        		impactOffset = null;
        		}
        	}
        	
        	drawHoles(c);
        }
        
        public void drawFaces(Canvas c)
        {
        	if(isEnableFaceDetection() &&
        				eyesMidPts!=null)
    		{
    			for (int i = 0; i < eyesMidPts.length; i++)
    	        {
    	                if (eyesMidPts[i] != null)
    	                {
    	                	//int yOffset = 0;
	                	    //widen hitspot slightly
	                	    int xOffset =  (int) (CustomTarget.WIDTH_LEVEL_1.x*0.1);
	                	    
	                	    headshotRadius = eyesDistance[i] / 2 + xOffset;
	                	    
	                	    float cX = eyesMidPts[i].x + targetOffset.x;
	                	    float cY = eyesMidPts[i].y + targetOffset.y;
	                	    
	                	    circlePts[0] = new PointF(cX,
	                	    						cY);
	                	    
	                	    if(isEnableHitboxes()){
	                	    	c.drawCircle(cX, 
                    					cY, 
                    					headshotRadius, 
                    					pHeadshot);
	                	    }
	                	    
	                	    if(isEnableHeadshots() &&
	                	    		gunDischarge && impactOffset!=null){
	                	    	if(isHeadShot(impactOffset.x,impactOffset.y)){
	                	    		onHeadshot();
	                	    	}
	                	    }
    	                }
    	        }
    		}
        }
        
        private boolean isHeadShot(double x, double y){
    		
    		final double circleX = circlePts[0].x;
    		final double circleY = circlePts[0].y;
    		
    		Double d = Math.sqrt((x-circleX)*(x-circleX) + (y-circleY)*(y-circleY));
    		
    		if(Firearm.DBG_MODE)
    			Log.i("Target","Impact distance from circle center: "+d+" ("+x+","+y+")");
    		
    		if(d <= headshotRadius){
    			if(Firearm.DBG_MODE)
    				Log.w("Target","Got Headshot!");
    			return true;
    		} else {
    			if(Firearm.DBG_MODE)
    				Log.w("Target","No Headshot.");
    		}
    		
    		return false;
    	}
        
        public void changeTargetResource(int resId)
        {
        	if(targetView!=null)
        	{
        		targetView.recycle();
        		targetView = null;
        	}
        	
        	targetView = BitmapFactory.decodeResource(ctx.getResources(),
        									resId);
        }
        
        public void refreshTarget()
        {
        	for (int i = 0; i < bulletHoles.length; i++) {
				bulletHoles[i] = null;
			}
        }
        
		public FirearmThread(SurfaceHolder surfaceHolder, Context context, Bitmap[] anim) {
			
            mSurfaceHolder = surfaceHolder;
            currentAnimation = anim;
            
            bgImage = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.range);
            
            targetList = new MovingTarget[3];
            
            int width = ((Activity)context).getWindowManager().getDefaultDisplay().getWidth();
            //width+=25;
            screenWidth = Util.pixelsToDip(context,width);
            
            //bgImage.
            targetStack = new ArrayList<MovingTarget>(5);
            markerStack = new ArrayList<PointMarker>(5);
            
            //mSurfaceHolder.setFormat(PixelFormat.RGBA_8888);
            
            gsColor = Color.rgb(25, 255, 0);
            
			mPaint = new Paint();
			mPaint.setAntiAlias(false);
			mPaint.setDither(true);
			
			pFlash = new Paint();
			pFlash.setAntiAlias(true);
			pFlash.setDither(true);
			
			pHeadshot = new Paint();
			pHeadshot.setAntiAlias(false);
			pHeadshot.setDither(false);
			pHeadshot.setStyle(Paint.Style.STROKE);
			pHeadshot.setColor(Color.RED);
			pHeadshot.setStrokeWidth(1);
			
			tPaint = new Paint();
			//tPaint.setShadowLayer(2, 1, 1, Color.BLACK);
			tPaint.setTextAlign(Paint.Align.CENTER);
			tPaint.setAntiAlias(true);
			tPaint.setTextScaleX(1.2f);
			
			targetOffset = new PointF();
			targetHolderOffset = new PointF();
			
			mLastTime = System.currentTimeMillis() + 100;
		}
		
        public synchronized void restoreState(Bundle savedState) {
            synchronized (mSurfaceHolder) {
                
            }
        }
        
        public Bundle saveState(Bundle map) {
            synchronized (mSurfaceHolder) {
                
            }
            return map;
        }

        /* Callback invoked when the surface dimensions change. */
        public void setSurfaceSize(int width, int height) {
            // synchronized to make sure these all change atomically
            synchronized (mSurfaceHolder) {
                //mCanvasWidth = width;
                //mCanvasHeight = height;

                // don't forget to resize the background image
                //mBackgroundImage = Bitmap.createScaledBitmap(mBackgroundImage, width, height, true);
            }
        }
        
        /*protected void drawToCanvas() throws InterruptedException
        {
        	if(currentAnimation!=null)
        	{
        		if(currentAnimation.length>0)
        		{
        			int flashStartFrame = 2;
        			int flashLen = 2;
        			int flashCurrent = 0;
        			int gunDischargeFrame = 2;
        			
        			mainCanvas = null;
        			
        			for(int i=0;i<currentAnimation.length;i++)
        			{
        				if(animationStopped){
        					return;
        				}
        				
        				mainCanvas = mSurfaceHolder.lockCanvas(null);
        				
        				if(mainCanvas==null)
        					return;
        				
        				//draw background
        				mainCanvas.drawBitmap(bgImage, bgOffset.x, bgOffset.y, null);
        				
        				if(targetMode==AndroidGunz.TARGET_MODE_DEFAULT)
        				{
	        				drawTarget();
        				}
        				else
        				{
        					//draw background animations and anything else that moves behind
                    		//an idle animation
            				updateTargets();
        				}
        				
        				if(gunDischarge){
        					if(i==(gunDischargeFrame-1)){
        						Firearm.onDischarge();
        					}
        				}
        				
        				if(flashAnimation!=null && gunDischarge)
        				{
	        				if(i>=flashStartFrame-1 && i <= (flashLen+(flashStartFrame-2))){
	        					pFlash.setAlpha(255-(flashCurrent*100));
	        					mainCanvas.drawBitmap(flashAnimation[flashCurrent], flashOffset.x,
	        														flashOffset.y, pFlash);
	        					flashCurrent++;
	        				}
        				}
        				
        				if(targetMode!=AndroidGunz.TARGET_MODE_DEFAULT)
        				{
	        				//draw feedback animations to dissapate over the action
	        				drawMarkers();
        				}
        				
        				mainCanvas.drawBitmap(currentAnimation[i], 
        										this.marginLeft, 
        										this.marginTop,
        										mPaint);
        				
        				mSurfaceHolder.unlockCanvasAndPost(mainCanvas);
                		//AndroidGunz.mHandler.post(refreshGuncon);
        				mainCanvas = null;
                		
                		if(animationStopped){
        					return;
        				}
                		
        				Thread.sleep(35);
        			}
        		}
        	}
        	animationStopped=true;
        }
        */
        
        public boolean isPlaying()
        {
        	return(!animationStopped);
        }
        
        /*public void stopAnimation() throws InterruptedException
        {
        	if(mainCanvas==null)
        		mainCanvas = mSurfaceHolder.lockCanvas(null);
        	
        	mainCanvas.drawBitmap(bgImage, bgOffset.x, bgOffset.y, null);
        	
        	if(targetMode==AndroidGunz.TARGET_MODE_DEFAULT)
			{
				drawTarget();
			}
        	else {
        		//draw background animations and anything else that moves behind
        		//an idle animation
    			updateTargets();
    			drawMarkers();
        	}
			
			mainCanvas.drawBitmap(FirearmOld.fireAnimation[FirearmOld.fireAnimation.length-1],
										this.idleMargin.x, this.idleMargin.y, mPaint);
			
			Thread.sleep(35);
        }*/
        
        public boolean hasBeenStarted()
        {
        	return(started);
        }
        
        public void run() {
        	
        	gunThread.bgOffset = new PointF();
    		gunThread.bgOffset.x = ((gunThread.bgImage.getWidth()/2) - (getWidth()/2)) * -1;
    		gunThread.bgOffset.y = ((gunThread.bgImage.getHeight()/2) - (getHeight()/2)) * -1;
    		
    		if(targetMode==AndroidGunz.TARGET_MODE_DEFAULT)
    		{
    			if(targetView==null)
					targetView = BitmapFactory.decodeResource(ctx.getResources(),
															Target.DEFAULT_TARGET);
				
				if(bulletView==null)
					bulletView = BitmapFactory.decodeResource(ctx.getResources(),
										R.drawable.bhole1);
				
				if(targetHolderView==null){
					targetHolderView = BitmapFactory.decodeResource(ctx.getResources(),
							R.drawable.target_holder);
				}
				
				bulletHoles = new PointF[Target.MAX_BULLETHOLES];
				
    			gunThread.targetOffset = new PointF();
    			gunThread.targetOffset.x = ((gunThread.targetView.getWidth()/2) - (getWidth()/2)) * -1;
    			gunThread.targetOffset.y = Util.pixelsToDip(ctx,Target.OFFSET_LEVEL0);//((AndroidGunz.target0.getHeight()/2) - (AndroidGunz.guncon.getHeight()/2)) * -1;
    			
    			gunThread.targetHolderOffset = new PointF();
    			gunThread.targetHolderOffset.x = (gunThread.targetOffset.x - (gunThread.targetView.getWidth()/2)) + 
    												((getWidth()-gunThread.targetHolderView.getWidth())/2);
    			gunThread.targetHolderOffset.y = gunThread.targetOffset.y - (gunThread.targetHolderView.getHeight()-Util.pixelsToDip(ctx,5));
    		} else {
    			targetDown = BitmapFactory.decodeResource(ctx.getResources(),
						R.drawable.target_down);
    		}
    		
    		/*if(preloading){
    			GameController.onAnimationFinished();
    		}*/
    		
    		//weapon.setReady(true);
    		
    		weapon.loadPackage(getWidth(),getHeight());
			
			soundFireId = soundPool.load(ctx, R.raw.meu_shoot1, 1);
			soundReloadId = soundPool.load(ctx,R.raw.meu_reload, 1);
			soundMiscId = soundPool.load(ctx, R.raw.meu_slideback, 1);
			soundHeadshotId = soundPool.load(ctx, R.raw.headshot, 1);
			
			((AndroidGunz)ctx).onConfigParsed();
			
			preloading = false;
        	
    		Rect r = new Rect(0,0,getWidth(),getHeight());
    		
        	while(mRun)
        	{
        		Canvas c = null;
        		
        		if(!isSurfaceReady){
        			try {
						Thread.sleep(250);
					} catch (InterruptedException e) {
					}
        			continue;
        		}
        		
                try {
                    synchronized (mSurfaceHolder) {
                    	c = mSurfaceHolder.lockCanvas();
                    	c.drawColor(Color.BLACK);
                    	
                    	c.drawBitmap(levelBackground, null, r, null);
                    	
                    	switch(targetMode){
                    		case(AndroidGunz.TARGET_MODE_DEFAULT):
                    			drawTarget(c);
                    			break;
                    		default:
                    			renderTargets(c);
                    			drawMarkers(c);
                    			break;
                    	}
                    	
                    	weapon.render(c);
                    }
                } finally {
                	if (c != null) {
                		mSurfaceHolder.unlockCanvasAndPost(c);
                    }
                }

        	}
        }

	}
	
	public static void onDischarge(int roundcount)
	{
		soundPool.play(soundFireId, 1.0f, 1.0f, 1, 0, 1.0f);
		
		t.schedule(new TimerTask() {
			public void run() {
				vibrator.vibrate(GUN_VIBRATE_TIME);
			}
		}, 1);
		
		if(!config.johnWoo)
			updateRoundCount(roundcount);
	}
	
	public static void onAnimationFinished() {
		if(rAnimationFinished!=null){
			AndroidGunz.mHandler.post(new Runnable() {
				
				public void run() {
					rAnimationFinished.run();
					//rAnimationFinished = null;
				}
			});
		}
	}

	
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		gunThread.setSurfaceSize(width, height);
	}
	
	public void setFlashOffset(int x, int y)
	{
		gunThread.flashOffset = new PointF(Util.pixelsToDip(ctx,x), 
										Util.pixelsToDip(ctx,y+(int)gunThread.idleMargin.y));
	}
	
	public void setGunDischarge(boolean discharge)
	{
		gunThread.gunDischarge = discharge;
	}
	
	public void setFlashAnimation(Bitmap[] b)
	{
		gunThread.flashAnimation = b;
	}
	
	public void setIdleAnimation(Bitmap b)
	{
		//gunThread.idleAnimation = b;
		gunThread.idleMargin.y = getHeight() - b.getHeight();
		gunThread.idleMargin.x = getWidth() - b.getWidth();
	}
	
	public float getIdleAnimationMarginTop()
	{
		return(gunThread.idleMargin.x);
	}
	
	public void setAnimationFrames(Bitmap[] b, String name)
	{
		if(currentAnimationName!=null && currentAnimationName.equals(name)){
			return;
		}
		
		currentAnimationName = name;
		
		gunThread.marginTop = getHeight() - b[0].getHeight();
		gunThread.marginLeft = getWidth() - b[0].getWidth();
		
		gunThread.currentAnimation = null;
		gunThread.currentAnimation = b;
	}

	public void preload(){
		//gunThread.preload();
		
		gunThread.gunDischarge = false;
		
		levelBackground = BitmapFactory.decodeResource(ctx.getResources(),
				R.drawable.range);
		
		//AndroidGunz.onPreloadFinished();
	}
	
	public void startAnimation(Bitmap[] b, String name)
	{ 
		setAnimationFrames(b,name);
		startAnimation();
	}
	
	public void startAnimation()
	{
		if(surfaceReady)
		{
			gunThread.animationStopped = false;
		}
	}
	
	public void stopAnimation()
	{
		gunThread.interrupt();
	}
	
	public void setCustomTarget(Bitmap b){
		unsetCustomTarget();
		gunThread.customTarget = b;
	}
	
	public void unsetCustomTarget(){
		if(gunThread.customTarget != null)
		{
			gunThread.customTarget.recycle();
			gunThread.customTarget = null;
		}
	}
	
	public int detectCurrentFaces()
	{
		  if(gunThread.customTarget==null || gunThread.customTarget.isRecycled())
				return 0;
		
		  FaceDetector Fdet = new FaceDetector(gunThread.customTarget.getWidth(), 
				  								gunThread.customTarget.getHeight(), 
				  								Target.FD_MAX_FACES);
		  
		  if(Firearm.DBG_MODE)
			  Log.w("Target","detecting faces of current target, res: "+gunThread.customTarget.getWidth()+","+gunThread.customTarget.getHeight());
		  
		  Face[] faces = new Face[Target.FD_MAX_FACES];
		  
		  int faceCount = Fdet.findFaces(gunThread.customTarget, faces);
		  
		  if(faceCount>0)
		  {
			  if(Firearm.DBG_MODE)
				  Log.w("Target","Found faces, count: "+faceCount);
			  
			  for (int i = 0; i < faceCount; i++)
			  {
				  PointF eyesMP = new PointF();
				  faces[i].getMidPoint(eyesMP);
				  eyesMidPts[i] = eyesMP;
				  eyesDistance[i] = faces[i].eyesDistance();
				  
				  if(Firearm.DBG_MODE)
					  Log.w("Target","Face #"+i+" , midpoint at: "+eyesMP.x+","+eyesMP.y);
			  }
			  
			  setFaceData(eyesMidPts, 
			  				eyesDistance);
		  }
		  else 
		  {
			  clearFaceData();
		  }
		  
		  return(faceCount);
	}
	
	public void setFaceData(PointF[] midPts,float[] eyeD){
		eyesMidPts = midPts;
		eyesDistance = eyeD;
		circlePts = new PointF[midPts.length];
		
		//note: this was only used when faces were detected out of a full-sized copy of the img
		//float xRatio = AndroidGunz.guncon.getWidth()*1.0f / CustomTarget.WIDTH_LEVEL_1.x;
		//float yRatio = AndroidGunz.guncon.getHeight()*1.0f / CustomTarget.WIDTH_LEVEL_1.y;
		//highlightSizeRatio[0] = 1.0f;
		//highlightSizeRatio[1] = 1.0f;
	}
	
	public void clearFaceData(){
		eyesMidPts = null;
		eyesDistance = null;
	}
	
	
	public void surfaceCreated(SurfaceHolder holder) {
		
		//Canvas c = holder.lockCanvas();
		//c.drawColor(Color.TRANSPARENT);
		//holder.unlockCanvasAndPost(c);
		
		gunThread.mSurfaceHolder = holder;
		gunThread.mRun = true;
		surfaceReady = true;
		gunThread.isSurfaceReady = true;
		
		if(!gunThread.isAlive())
			gunThread.start();
	}

	
	public void surfaceDestroyed(SurfaceHolder holder) {
		
		gunThread.isSurfaceReady = false;
		surfaceReady = false;
		//gunThread.mRun = false;
		
		//gunThread.interrupt();
		//gunThread.stop();
		//boolean retry = true;
		
		//gunThread.interrupt();
		//gunThread.mRun = false;
		
		/*while (retry) {
            try {
            	gunThread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }*/

		
       /* try {
        	//gunThread.join();
        	gunThread.stop();
        } catch (InterruptedException e) {
        	if(gunThread.isAlive())
        		gunThread.stop();
        }*/
	}

	public static void onReload() {
		soundPool.play(soundReloadId, 1.0f, 1.0f, 1, 0, 1.0f);
		t.schedule(new TimerTask() {
			public void run() {
				vibrator.vibrate(GUN_VIBRATE_TIME_RELOAD);
			}
		}, 550);
	}
	
	public void destroy() {
		//surfaceReady = false;
		//gunThread.isSurfaceReady = false;
		gunThread.mRun = false;
		gunThread.animationStopped = true;
		
		gunThread.interrupt();
		try {
			gunThread.join();
		} catch (InterruptedException e) {
		}
		
		gunThread.currentAnimation = null;
		
		if(gunThread.flashAnimation!=null)
		{
			/*for (Bitmap b : gunThread.flashAnimation) {
				b.recycle();
				b = null;
			}*/
			//gunThread.flashAnimation = null;
		}
		
		if(targetMode==AndroidGunz.TARGET_MODE_DEFAULT)
		{
			if(gunThread.targetView != null)
			{
				gunThread.targetView.recycle();
				gunThread.targetView = null;
			}
			if(gunThread.bulletView != null)
			{
				gunThread.bulletView.recycle();
				gunThread.bulletView = null;
			}
			if(gunThread.customTarget!=null){
				gunThread.customTarget.recycle();
				gunThread.customTarget = null;
			}
			if(gunThread.targetHolderView!=null){
				gunThread.targetHolderView.recycle();
				gunThread.targetHolderView = null;
			}
		}
		else
		{
			if(gunThread.hostageView!=null){
				gunThread.hostageView.recycle();
				gunThread.hostageView = null;
			}
			
			if(gunThread.hostageView2 != null){
				gunThread.hostageView2.recycle();
				gunThread.hostageView2 = null;
			}
			
			if(gunThread.hostageView3 != null){
				gunThread.hostageView3.recycle();
				gunThread.hostageView3 = null;
			}
			
			if(gunThread.hostageView4 != null){
				gunThread.hostageView4.recycle();
				gunThread.hostageView4 = null;
			}
			
			if(gunThread.targetDown != null){
				gunThread.targetDown.recycle();
				gunThread.targetDown = null;
			}
			
			for (MovingTarget m : gunThread.targetList) {
				if(m.sprite!=null)
				{
					m.sprite.recycle();
					m.sprite = null;
				}
			}
			gunThread.targetList = null;
		}
		
		if(gunThread.bgImage!=null)
		{
			gunThread.bgImage.recycle();
			gunThread.bgImage = null;
		}
		
		//gunThread.mSurfaceHolder = null;
		rAnimationFinished = null;
		currentAnimationName = null;
		//gunThread.idleAnimation = null;
		//gunThread.stop();
	}

	public static void updateRoundCount(int roundsLeft) {
		AndroidGunz.setRoundCount(roundsLeft);
	}

	public void setEnableHeadshots(boolean enableHeadshots) {
		this.enableHeadshots = enableHeadshots;
	}

	public boolean isEnableHeadshots() {
		return enableHeadshots;
	}

	public void setEnableFaceDetection(boolean enableFaceDetection) {
		this.enableFaceDetection = enableFaceDetection;
	}

	public boolean isEnableFaceDetection() {
		return enableFaceDetection;
	}

	public void setEnableHitboxes(boolean enableHitboxes) {
		this.enableHitboxes = enableHitboxes;
	}

	public boolean isEnableHitboxes() {
		return enableHitboxes;
	}
	public  void setJohnWooMode(boolean enable){
		config.johnWoo = enable;
	}
	public  void setAutoReload(boolean enable){
		config.autoReload = enable;
	}
}