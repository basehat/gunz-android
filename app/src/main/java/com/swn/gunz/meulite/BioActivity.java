package com.swn.gunz.meulite;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class BioActivity extends Activity {

	public static boolean OOM_ERROR = false;
	
	private static int currentPage = 1;
	private static final int pageCount = 3;
	private static MediaPlayer mp = null;
	//private static LinearLayout[] specPages;
	private static int specCount = 3;
	private ImageView bCloseInfo;
	private ImageView bOpenInfo;
	private ImageView bFlybyStart;
	
	private static Context c;
	private static Animation pageOut;
	private static Animation pageIn;
	private BioView bioView;
	private Menus menuController;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		c = this;
		
		
		menuController = new Menus(this);
		//getWindow().setFormat(PixelFormat.RGBA_8888);
		
		setContentView(R.layout.bioview);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		
		/*try{
		} catch(OutOfMemoryError e){
			Toast.makeText(this, R.string.t_message_oom, Toast.LENGTH_LONG).show();
			finish();
		}*/
		
		bioView = (BioView)findViewById(R.id.bioview);
		
		bFlybyStart = (ImageView)findViewById(R.id.b_flyby_start);
		bFlybyStart.setImageResource(android.R.drawable.ic_menu_rotate);
		
		bFlybyStart.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				if(bioView.bioThread.animationStopped){
					bioView.startAnimation();
				} else {
					bioView.stopAnimation();
				}
			}
		});
		
		Typeface crassFont = Typeface.createFromAsset(getAssets(), "fonts/crass.ttf");
		
		FirearmBio fb = Util.getBio(this,FireArms.MEU_1911);
		
		bCloseInfo = (ImageView)findViewById(R.id.b_closeinfo);
		bOpenInfo = (ImageView)findViewById(R.id.b_openinfo);
		
		bCloseInfo.setImageResource(android.R.drawable.ic_menu_close_clear_cancel);
		
		TextView label1 = (TextView)findViewById(R.id.label_1);
		TextView vCname = (TextView)findViewById(R.id.v_cname);
		TextView vCrange = (TextView)findViewById(R.id.v_range);
		TextView vCfps = (TextView)findViewById(R.id.v_fps);
		TextView vCpower = (TextView)findViewById(R.id.v_power);
		TextView vUser   = (TextView)findViewById(R.id.v_used);
		TextView vWars   = (TextView)findViewById(R.id.v_war);
		TextView vDesign   = (TextView)findViewById(R.id.v_designer);
		TextView vYear   = (TextView)findViewById(R.id.v_year);
		TextView vWeight   = (TextView)findViewById(R.id.v_weight);
		TextView vLength   = (TextView)findViewById(R.id.v_length);
		TextView vLengthb   = (TextView)findViewById(R.id.v_lengthb);
		TextView vFeed   = (TextView)findViewById(R.id.v_feed);
		TextView vFireRate   = (TextView)findViewById(R.id.v_rof);
		
		label1.setTypeface(crassFont);
		
		vCname.setText(fb.cartdrigeName);
		vCrange.setText(fb.cartridgeRange);
		vCfps.setText(fb.cartdrigeVelocity);
		vCpower.setText(fb.cartridgePower);
		vUser.setText(fb.usedBy);
		vWars.setText(fb.usedWars);
		vDesign.setText(fb.designer);
		vYear.setText(fb.designedYear);
		vWeight.setText(fb.weight);
		vLength.setText(fb.length);
		vLengthb.setText(fb.lengthBarrel);
		vFeed.setText(fb.feed);
		vFireRate.setText(fb.fireRate);
		
		fb.destroy();
		
		ImageView bBack = (ImageView)findViewById(R.id.b_back);
		ImageView bFwd = (ImageView)findViewById(R.id.b_fwd);
		
		bOpenInfo.setImageResource(R.drawable.ic_menu_start_conversation);
		bOpenInfo.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				try{
					if(mp!=null && mp.isPlaying()){
						mp.stop();
						mp.release();
					}
				} catch(Exception e){
				}
				
				mp = MediaPlayer.create(BioActivity.this, R.raw.meu_slideback);
				mp.setOnCompletionListener(new OnCompletionListener() {
					
					public void onCompletion(MediaPlayer mp) {
						mp.release();
					}
				});
				mp.start();
				
				LinearLayout infoPanel = (LinearLayout)findViewById(R.id.infoPanel);
				
				if(infoPanel.getVisibility()!=View.VISIBLE){
					infoPanel.setVisibility(View.VISIBLE);
					//infoPanel.bringToFront();
					bOpenInfo.setVisibility(View.INVISIBLE);
					bFlybyStart.setVisibility(View.INVISIBLE);
				}
			}
		});
		bCloseInfo.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				LinearLayout infoPanel = (LinearLayout)findViewById(R.id.infoPanel);
				if(infoPanel.getVisibility()==View.VISIBLE){
					infoPanel.setVisibility(View.GONE);
					bOpenInfo.setVisibility(View.VISIBLE);
					bFlybyStart.setVisibility(View.VISIBLE);
				}
			}
		});
		
		
		//specPages = new LinearLayout[pageCount];
		
		//specPages[0] = (LinearLayout)findViewById(R.id.specs1);
		//specPages[1] = (LinearLayout)findViewById(R.id.specs2);
		//specPages[2] = (LinearLayout)findViewById(R.id.specs3);
		
		bFwd.setOnClickListener(new onSpecPageChange(true));
		bBack.setOnClickListener(new onSpecPageChange(false));
		
		pageOut = AnimationUtils.loadAnimation(c, android.R.anim.slide_out_right);
		pageIn = AnimationUtils.loadAnimation(c, android.R.anim.slide_in_left);
		pageOut.setFillAfter(true);
		pageIn.setFillAfter(true);
		pageOut.setInterpolator(this, android.R.anim.accelerate_interpolator);
		pageIn.setInterpolator(this, android.R.anim.accelerate_interpolator);
		
		pageOut.setAnimationListener(new AnimationListener() {
			
			public void onAnimationStart(Animation animation) {
			}
			
			public void onAnimationRepeat(Animation animation) {
			}
			
			public void onAnimationEnd(Animation animation) {
				switch(currentPage){
					case(1):
						((LinearLayout)findViewById(R.id.specs1)).startAnimation(pageIn);
						break;
					case(2):
						((LinearLayout)findViewById(R.id.specs2)).startAnimation(pageIn);
						break;
					case(3):
						((LinearLayout)findViewById(R.id.specs3)).startAnimation(pageIn);
						break;
				}
				
			}
		});
		
		bioView.post(new Runnable() {
			public void run() {
				LinearLayout infoPanel = (LinearLayout)findViewById(R.id.infoPanel);
				infoPanel.setVisibility(View.GONE);
			}
		});
	}
	
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode==KeyEvent.KEYCODE_BACK){
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	
	protected void onDestroy() {
			pageOut = null;
			pageIn = null;
			if(mp!=null)
			{
				try{
					mp.stop();
					mp.release();
				} catch(IllegalStateException e){
				}
			}
			
			bioView.destroy();
			
		super.onDestroy();
	}
	
	private static class onSpecPageChange implements View.OnClickListener {
		private boolean increment = true;
		public onSpecPageChange(boolean inc) {
			this.increment = inc;
		}
		
		public void onClick(View v) {
			
			switch(currentPage){
				case(1):
					((LinearLayout)((Activity)c).findViewById(R.id.specs1)).startAnimation(pageOut);
					break;
				case(2):
					((LinearLayout)((Activity)c).findViewById(R.id.specs2)).startAnimation(pageOut);
					break;
				case(3):
					((LinearLayout)((Activity)c).findViewById(R.id.specs3)).startAnimation(pageOut);
					break;
			}
			
			//specPages[currentPage-1].startAnimation(pageOut);
			
			if(this.increment)
				currentPage++;
			else
				currentPage--;
			
			if(currentPage>pageCount){
				currentPage = 1;
			} 
			if(currentPage<1){
				currentPage = pageCount;
			} 
		}
	}
	
	
	protected Dialog onCreateDialog(int id) {
		return menuController.prepareDialog(id);
	}
	
}
