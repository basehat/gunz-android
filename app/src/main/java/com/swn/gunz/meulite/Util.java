package com.swn.gunz.meulite;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Environment;
import android.util.Log;
import android.util.Xml;

public class Util {
	
	protected static final String WEAPON_DESERT_EAGLE = "deagle";
	protected static final String WEAPON_BERETTA_M9 = "m9";
	protected static final String WEAPON_MEU_1911 = "meu";
	
	private static final String ANIMATION_FRAME_TAG = "frame";
	private static final int[] FROM_COLOR = new int[]{25, 254, 1};
	private static final int THRESHOLD = 1; 
	
	public final static String GUNZ_GUN_DIRECTORY_GLOBAL = "/GunZ/Guns";
	public final static String GUNZ_GUN_DIRECTORY_CACHE = "cache/";
	
	public final static String GUNZ_PACKAGE_FILENAME = "gun.zip";
	
	public static final String CONFIG_TAGNAME_MAGAZINECAPACITY = "magazineCapacity";
	public static final String CONFIG_TAGNAME_DISCHARGEANIMS = "dischargeAnimations";
	public static final String CONFIG_TAGNAME_MFLASHOFFSETX = "muzzleFlashOffsetX";
	public static final String CONFIG_TAGNAME_MFLASHOFFSETY = "muzzleFlashOffsetY";
	public static final String CONFIG_TAGNAME_MFLASHENABLED = "hasMuzzleFlash";
	
	protected static final String ANIM_STAGE_SHOOT = "shoot";
	protected static final String ANIM_STAGE_SHOOTLAST = "shootlast";
	protected static final String ANIM_STAGE_DRAW = "draw";
	protected static final String ANIM_STAGE_RELOAD = "reload";
	protected static final String ANIM_SOUND_SLIDEBACK = "slideback";
	
	public static final int ANIM_LEVEL_FIRST = 1;
	public static final String DRAWABLE_PREFIX = "x_";
	public static final String CONFIG_PREFIX = "config_";
	private static final String BIOXML_PREFIX = "bio_";
	
	private static final String BIO_TAGNAME_USEDBY = "usedBy";
	private static final String BIO_TAGNAME_USEDWAR = "usedWars";
	private static final String BIO_TAGNAME_AUTHOR = "designer";
	private static final String BIO_TAGNAME_YEAR = "designedYear";
	private static final String BIO_TAGNAME_WEIGHT = "weight";
	private static final String BIO_TAGNAME_LENGTH = "length";
	private static final String BIO_TAGNAME_LENGTHB = "lengthBarrel";
	private static final String BIO_TAGNAME_CNAME = "cartridgeName";
	private static final String BIO_TAGNAME_CRANGE = "cartridgeRange";
	private static final String BIO_TAGNAME_CFPS = "cartridgeVelocity";
	private static final String BIO_TAGNAME_CPOWER = "cartridgePower";
	private static final String BIO_TAGNAME_ACTION = "actionType";
	private static final String BIO_TAGNAME_ROF = "fireRate";
	private static final String BIO_TAGNAME_FEED = "feedType";
	private static final String BIO_TAGNAME_DESC = "description";
	private static final String BIO_TAGNAME_HISTORY = "history";
	
	private int currentLevel = ANIM_LEVEL_FIRST;
	
	public static int installGunPack(Context c)
	{
		String state = Environment.getExternalStorageState();
		
		if (Environment.MEDIA_MOUNTED.equals(state)) {
	        //continue
	    } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
	        return MenuActivity.INSTALL_ERROR_SDCARD;
	    } else {
	    	return MenuActivity.INSTALL_ERROR_SDCARD;
	    }
		
		String tmpDir = Environment.getExternalStorageDirectory().getAbsolutePath();
		
		tmpDir += GUNZ_GUN_DIRECTORY_GLOBAL + "/" + AndroidGunz.GUN_ID + "/";
		File tmpDirFile = new File(tmpDir);
		
		//if(tmpDirFile.exists())
		//	return true;
		
		if(tmpDirFile.mkdirs()){
			
			File outFile = new File(tmpDir+GUNZ_PACKAGE_FILENAME);
			
			outFile.mkdirs();
			
			if(outFile.exists())
				outFile.delete();
			try
			{
				if(outFile.createNewFile())
				{
						//place zip package
						BufferedInputStream bis = 
								new BufferedInputStream(c.getResources().openRawResource(R.raw.gun),8192*8);
						
						
						BufferedOutputStream bos= new BufferedOutputStream(new FileOutputStream(outFile));
						
						for (int i = bis.read(); i != -1; i = bis.read()) {
							bos.write(i);
						}
						
						bos.flush();
						bos.close();
						bis.close();
						
						return MenuActivity.INSTALL_SUCCESS;
					} 
				}
			catch(IOException e){
				e.printStackTrace();
			}
			//no need to extract yet
			//return FireArms.extractPackage(tmpDir, false);
		} else {
			if(tmpDirFile.exists()){
				return MenuActivity.INSTALL_ERROR_EXISTS;
			}
		}
		
		return MenuActivity.INSTALL_ERROR_SDCARD;
	}
	
	public static float pixelsToDip(Context c, int px){
		float dip = (px * c.getResources().getDisplayMetrics().density + 0.5f);
		return dip;
	}
	
	/**
	 * convert an animationDrawable to a custom MyAnimationDrawable object
	 * which supports extended features.
	 * 
	 * @param resId
	 * @return
	 */
	public static MyAnimationDrawable convertAnimationDrawable(Context c, int resId){
		MyAnimationDrawable dest = new MyAnimationDrawable();
		AnimationDrawable source = (AnimationDrawable)c.getResources().getDrawable(resId);
		int x = source.getNumberOfFrames();
		
		for (int i = 0; i < x; i++) {
			//Bitmap srcFrame = ((BitmapDrawable)source.getFrame(i)).getBitmap();
			//dest.addFrame(new BitmapDrawable(srcFrame.copy(Bitmap.Config.ARGB_8888, true)), 100);
			dest.addFrame(source.getFrame(i),100);
		}
		
		source.setCallback(null);
		source = null;
		
		return dest;
	}
	
	public static Bitmap[] getDefaultMuzzleFlashBitmap(Context c)
	{
		Bitmap[] ret = new Bitmap[2];
		
		ret[0] = ((BitmapDrawable)c.getResources().getDrawable(R.drawable.muzzleflash3)).getBitmap();
		ret[1] = ((BitmapDrawable)c.getResources().getDrawable(R.drawable.muzzleflash4)).getBitmap();
		
		return ret;
	}
	
	public static Bitmap[] getDefaultBioBitmap(Context c)
	{
		//AnimationDrawable source = (AnimationDrawable)c.getResources().getDrawable(R.drawable.x_meu_flyby);
		int size = 44;//source.getNumberOfFrames();
		//source = null;
		
		Bitmap[] ret = new Bitmap[size];
		
		for(int i=0;i<size;i++){
			int resId = c.getResources().getIdentifier("z_anim_meuflyby_"+(i+1), "drawable", c.getPackageName());
			ret[i] = BitmapFactory.decodeResource(c.getResources(), resId);
		}
		
		return ret;
	}
	
	public static String[] parseXmlAnimation(Context c, int xmlResId){
		
		XmlResourceParser xr = c.getResources().getXml(xmlResId);
		ArrayList<String> ret = new ArrayList<String>();
		
		try {
			while (xr.getEventType() != XmlPullParser.END_DOCUMENT) {
				if (xr.getEventType() == XmlPullParser.START_TAG) {
							if(xr.getName().equals(ANIMATION_FRAME_TAG)){
								ret.add(xr.getAttributeValue(null, "drawable"));
							}
				        } else if (xr.getEventType() == XmlPullParser.END_TAG) {
				        }
				xr.next();
			}
		} catch (XmlPullParserException e) {
			if(AndroidGunz.DBG_MODE)
				e.printStackTrace();
		} catch (IOException e) {
			if(AndroidGunz.DBG_MODE)
				e.printStackTrace();
		}
		
		xr.close();
		
		return ret.toArray(new String[0]);
	}
	
	public static Bitmap[] getAnimationFrames(Context c, int xmlResId)
	{
		String[] frameNames = Util.parseXmlAnimation(c, xmlResId);
		Bitmap ret[] = new Bitmap[frameNames.length]; 
		int x = 0;
		for (String s : frameNames) {
			if(s!=null){
				int resId = c.getResources().getIdentifier(s, "drawable", c.getPackageName());
				BitmapFactory.Options bpo = new BitmapFactory.Options();
				bpo.inDither=false;
				bpo.inPreferredConfig = Config.RGB_565;

				ret[x] = BitmapFactory.decodeResource(c.getResources(), resId, bpo);
				x++;
			}
		}
		
		return ret;
	}
	
	/**
	 * convert an animationDrawable to a Bitmap[]
	 * which supports extended features.
	 * 
	 * @param resId
	 * @return
	 */
	public static Bitmap[] convertAnimationDrawableToBitmap(Context c, int resId){
		AnimationDrawable source = (AnimationDrawable)c.getResources().getDrawable(resId);
		int x = source.getNumberOfFrames();
		Bitmap[] dest = new Bitmap[x];
		
		for (int i = 0; i < x; i++) {
			dest[i] = ((BitmapDrawable)source.getFrame(i)).getBitmap();
			//dest.addFrame(source.getFrame(i),100);
		}
		
		source.setCallback(null);
		source = null;
		c = null;
		return dest;
	}
	
	public static FirearmConfig parseConfig(Context c){
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		
		FirearmConfig config = new FirearmConfig();
		
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e1) {
			if(AndroidGunz.DBG_MODE)
				e1.printStackTrace();
			builder = null;
		}
		
		if(builder==null)
			return config;
		
		try {
			String ident = CONFIG_PREFIX+GameController.WEAPON_MEU_1911;
			int resId = c.getResources().getIdentifier(ident, "raw", c.getPackageName());
			Document document = builder.parse(c.getResources().openRawResource(resId));
			
			String magCapacity = document.getElementsByTagName(CONFIG_TAGNAME_MAGAZINECAPACITY).item(0).getFirstChild().getNodeValue();
			String dischargeAnimations = document.getElementsByTagName(CONFIG_TAGNAME_DISCHARGEANIMS).item(0).getFirstChild().getNodeValue();
			String mFlashOffsetX = document.getElementsByTagName(CONFIG_TAGNAME_MFLASHOFFSETX).item(0).getFirstChild().getNodeValue();
			String mFlashOffsetY = document.getElementsByTagName(CONFIG_TAGNAME_MFLASHOFFSETY).item(0).getFirstChild().getNodeValue();
			String mFlashEnabled = document.getElementsByTagName(CONFIG_TAGNAME_MFLASHENABLED).item(0).getFirstChild().getNodeValue();
			
			if(AndroidGunz.DBG_MODE){
				Log.i("Firearm","Loading config");
				Log.i("Firearm","Found magazine capacity: "+magCapacity);
				Log.i("Firearm","Found discharge anims: "+dischargeAnimations);
				Log.i("Firearm","Found muzzle flash offset x: "+mFlashOffsetX);
				Log.i("Firearm","Found muzzle flash offset y: "+mFlashOffsetY);
			}
			
			config.magazineCap 		  = Integer.parseInt(magCapacity);
			config.dischareAnimations = Integer.parseInt(dischargeAnimations);
			config.muzzleFlashOffsetX = Integer.parseInt(mFlashOffsetX);
			config.muzzleFlashOffsetY = Integer.parseInt(mFlashOffsetY);
			config.hasMuzzleFlash 	  = mFlashEnabled.equals("false")?false:true;
			
			//restore saved options
			config.johnWoo = MenuActivity.getOptions(c).getSwitch(Options.PREF_KEY_OPTION_JOHNWOO);
			
		} catch (Exception e) {
			if(AndroidGunz.DBG_MODE)
				e.printStackTrace();
			factory = null;
		}
		factory = null;
		
		return config;
	}
	
	 /*private static Drawable greenscreenDrawable(Drawable d)
     {
        int to = Color.TRANSPARENT;

        //Need to copy to ensure that the bitmap is mutable.
        Bitmap src = ((BitmapDrawable) d).getBitmap();
        //Bitmap bitmap = src.copy(Bitmap.Config.RGB_565, true);
        //src = null;
        for(int x = 0;x < src.getWidth();x++)
            for(int y = 0;y < src.getHeight();y++)
                if(matchPixel(src.getPixel(x, y))) 
                    src.setPixel(x, y, to);
        
        return new BitmapDrawable(src);
     }

	private static boolean matchPixel(int pixel)
    {
        //There may be a better way to match, but I wanted to do a comparison ignoring
        //transparency, so I couldn't just do a direct integer compare.
        return Math.abs(Color.red(pixel) - FROM_COLOR[0]) < THRESHOLD &&
            Math.abs(Color.green(pixel) - FROM_COLOR[1]) < THRESHOLD &&
            Math.abs(Color.blue(pixel) - FROM_COLOR[2]) < THRESHOLD;
    }*/
	
	public static MediaPlayer createOneshot(Context c,int res,boolean play)
	{
		MediaPlayer mp = MediaPlayer.create(c,res);
		
		if(mp==null)
			return null;
		
		mp.setOnCompletionListener(AndroidGunz.mpComplete);
		mp.setOnErrorListener(AndroidGunz.mpError);
		
		if(play){
			mp.start();
			return null;
		}
		
		return mp;
	}
	
	public static String[] parseXmlAnimation(File f)
	throws Exception
	{
		ArrayList<String> ret = new ArrayList<String>();
		FileInputStream fis = new FileInputStream(f);
		XmlPullParser xr = Xml.newPullParser();
	
		xr.setInput(fis, null);
		
		while (xr.getEventType() != XmlPullParser.END_DOCUMENT) {
			if (xr.getEventType() == XmlPullParser.START_TAG) {
						if(xr.getName().equals(ANIMATION_FRAME_TAG)){
							ret.add(xr.getAttributeValue(null, "drawable"));
						}
			        } else if (xr.getEventType() == XmlPullParser.END_TAG) {
			        }
			xr.next();
		}
		
		fis.close();
		
		return ret.toArray(new String[0]);
	}
	
	public static Bitmap[] getAnimationFromDisk(Context c, String gundir, String stageFolder)
	throws Exception
	{
		String path = gundir+GUNZ_GUN_DIRECTORY_CACHE;
		
		File xmlConfig = new File(path+FireArms.GUNZ_ZPACKAGE_FILENAME_CONFIG);
		if(!xmlConfig.isFile()){
			throw new Exception(xmlConfig.getAbsolutePath()+" does not exist!");
		}
		
		path += stageFolder;
		
		File xmlAnimation = new File(path+FireArms.GUNZ_ZPACKAGE_FILENAME_AXML);
		if(!xmlAnimation.isFile()){
			throw new Exception(xmlAnimation.getAbsolutePath()+" does not exist!");
		}
		
		String[] frameNames = Util.parseXmlAnimation(xmlAnimation);
		
		Bitmap[] ret = new Bitmap[frameNames.length]; 
		int x = 0;
		for (String s : frameNames) {
			if(s!=null){
				String tmpPath = path+s+FireArms.GUNZ_GUN_ANIMATION_EXTENSION;
				ret[x] = BitmapFactory.decodeFile(tmpPath);
				x++;
			}
		}
		
			return ret;
	}
	
	public static boolean deleteDir(File dir)
	{
		if (dir.isDirectory())
		{ 
			String[] children = dir.list();
			for (int i=0; i<children.length; i++) { 
				boolean success = deleteDir(new File(dir, children[i])); 
				if (!success)
						return false;
			}
		}
		// The directory is now empty so delete it
		return dir.delete(); 
	}
	
	public static void copyInputStream(InputStream in, OutputStream out)
	  throws IOException
	  {
	    byte[] buffer = new byte[1024];
	    int len;

	    while((len = in.read(buffer)) >= 0)
	      out.write(buffer, 0, len);

	    in.close();
	    out.close();
	  }
	
	public static Bitmap[] getAnimation(Context c,String stage){
		return Util.getAnimation(c,stage,1);
	}
	public static Bitmap[] getAnimation(Context c, String stage, int level){
		String ident;
		if(stage.equals(ANIM_STAGE_SHOOT)){
			if(level>Firearm.gunDischargeAnimsCount)
				level = Util.ANIM_LEVEL_FIRST;
			ident = Util.DRAWABLE_PREFIX+FireArms.MEU_1911+"_"+stage+level;
		}
		else{
			ident = Util.DRAWABLE_PREFIX+FireArms.MEU_1911+"_"+stage;
		}
		
		int xmlId = c.getResources().getIdentifier(ident, "xml", c.getPackageName());
		//String[] animationFrames = Util.parseXmlAnimation(c, resId2);
		return Util.getAnimationFrames(c, xmlId);
	}
	
	public static FirearmBio getBio(Context c, String weaponId){
		FirearmBio fb = new FirearmBio();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			String ident = BIOXML_PREFIX+weaponId;
			int resId = c.getResources().getIdentifier(ident, "raw", c.getPackageName());
			Document document = builder.parse(c.getResources().openRawResource(resId));
			fb.actionType = document.getElementsByTagName(BIO_TAGNAME_ACTION).item(0).getFirstChild().getNodeValue();
			fb.cartdrigeName = document.getElementsByTagName(BIO_TAGNAME_CNAME).item(0).getFirstChild().getNodeValue();
			fb.cartdrigeVelocity = document.getElementsByTagName(BIO_TAGNAME_CFPS).item(0).getFirstChild().getNodeValue();
			fb.cartridgePower = document.getElementsByTagName(BIO_TAGNAME_CPOWER).item(0).getFirstChild().getNodeValue();
			fb.cartridgeRange = document.getElementsByTagName(BIO_TAGNAME_CRANGE).item(0).getFirstChild().getNodeValue();
			fb.description = document.getElementsByTagName(BIO_TAGNAME_DESC).item(0).getFirstChild().getNodeValue();
			fb.designedYear = document.getElementsByTagName(BIO_TAGNAME_YEAR).item(0).getFirstChild().getNodeValue();
			fb.designer = document.getElementsByTagName(BIO_TAGNAME_AUTHOR).item(0).getFirstChild().getNodeValue();
			fb.fireRate = document.getElementsByTagName(BIO_TAGNAME_ROF).item(0).getFirstChild().getNodeValue();
			fb.history = document.getElementsByTagName(BIO_TAGNAME_HISTORY).item(0).getFirstChild().getNodeValue();
			fb.length = document.getElementsByTagName(BIO_TAGNAME_LENGTH).item(0).getFirstChild().getNodeValue();
			fb.lengthBarrel = document.getElementsByTagName(BIO_TAGNAME_LENGTHB).item(0).getFirstChild().getNodeValue();
			fb.usedBy = document.getElementsByTagName(BIO_TAGNAME_USEDBY).item(0).getFirstChild().getNodeValue();
			fb.usedWars = document.getElementsByTagName(BIO_TAGNAME_USEDWAR).item(0).getFirstChild().getNodeValue();
			fb.weight = document.getElementsByTagName(BIO_TAGNAME_WEIGHT).item(0).getFirstChild().getNodeValue();
			fb.feed = document.getElementsByTagName(BIO_TAGNAME_FEED).item(0).getFirstChild().getNodeValue();
		} catch(Exception e){
			
		}
		return fb;
	}
}
