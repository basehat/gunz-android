package com.swn.gunz.meulite;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

public class Menus {
	
	protected static final boolean DBG_MODE = false;
	
	protected static final int MENU_MAIN = 1;
	protected static final int MENU_TARGET = 2;
	protected static final int MENU_TARGETSELECT = 3;
	protected static final int MENU_OPTIONS = 4;
	protected static final int MENU_MUSIC = 5;
	protected static final int MENU_MODE = 6;
	protected static final int MENU_OOM = 7;
	protected static final int MENU_EXIT = 8;
	
	private static MediaPlayer mp = null;
	
	private static final int menuSoundId = R.raw.meu_slideback;

	protected static Dialog currentDlg;
	
	protected static int browseIdx = 1;
	
	private Context c;
	
	public Menus(Context ctx) {
		c = ctx;
	}
	
	private static void playSound(Context c){
		try
		{
			if(mp!=null && mp.isPlaying()){
				mp.stop();
				mp.release();
			}
			
			mp = MediaPlayer.create(c, menuSoundId);
			if(mp!=null){
				if(AndroidGunz.mpComplete!=null){
					mp.setOnCompletionListener(AndroidGunz.mpComplete);
				}
				mp.start();
			}
		} catch(IllegalStateException e){mp=null;}
	}
	
	protected Dialog prepareDialog(int id){
		//Context c = d.getContext();
		//Builder builder = new AlertDialog.Builder(c);
		final LayoutInflater inflater = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final Typeface arFont = Typeface.createFromAsset(c.getAssets(), "fonts/armalite.ttf");
		
		switch(id)
		{
			case(MENU_MODE):
				View layout6 = inflater.inflate(R.layout.dlg_mode,null);
			
				TextView bMain2 = (TextView)layout6.findViewById(R.id.b_main);
			
				ImageView bMode1 = (ImageView)layout6.findViewById(R.id.b_mode1);
				ImageView bMode2 = (ImageView)layout6.findViewById(R.id.b_mode2);
				ImageView bMode3 = (ImageView)layout6.findViewById(R.id.b_mode3);
				
				bMode1.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						Intent i = new Intent(c,AndroidGunz.class);
						i.putExtra(AndroidGunz.INTENT_EXTRA_GAMEMODE, 
									AndroidGunz.TARGET_MODE_DEFAULT);
						c.startActivity(i);
						((Activity)c).removeDialog(MENU_MODE);
					}
				});
				bMode2.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						Intent i = new Intent(c,AndroidGunz.class);
						i.putExtra(AndroidGunz.INTENT_EXTRA_GAMEMODE, 
									AndroidGunz.TARGET_MODE_ANIMATED);
						c.startActivity(i);
						((Activity)c).removeDialog(MENU_MODE);
					}
				});
				bMode3.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						Intent i = new Intent(c,AndroidGunz.class);
						i.putExtra(AndroidGunz.INTENT_EXTRA_GAMEMODE, 
									AndroidGunz.TARGET_MODE_ANIMATED2);
						c.startActivity(i);
						((Activity)c).removeDialog(MENU_MODE);
					}
				});
				
				
				bMain2.setTextColor(Color.parseColor("#ababab"));
				bMain2.setTypeface(arFont);
				currentDlg = new Dialog(c, R.style.GameMenu);
				currentDlg.setContentView(layout6);
			break;
			case(MENU_MAIN):
				
				View layout = inflater.inflate(R.layout.dlg_main,null);
				//LinearLayout ll = (LinearLayout)layout.findViewById(R.id.layout_root);
				
				TextView bMain = (TextView)layout.findViewById(R.id.b_main);
				TextView bTarget = (TextView)layout.findViewById(R.id.b_target);
				TextView bMusic = (TextView)layout.findViewById(R.id.b_music);
				TextView bOptions = (TextView)layout.findViewById(R.id.b_options);
				TextView bQuit = (TextView)layout.findViewById(R.id.b_quit);
				
				bMain.setTypeface(arFont);
				bTarget.setTypeface(arFont);
				bMusic.setTypeface(arFont);
				bOptions.setTypeface(arFont);
				bQuit.setTypeface(arFont);
				
				bTarget.setClickable(true);
				bTarget.setHapticFeedbackEnabled(false);
				bTarget.setSoundEffectsEnabled(false);
				//bTarget.setTextAppearance(context, resid)
				bTarget.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						playSound(v.getContext());
						AndroidGunz.onButtonTargetmenu();
					}
				});
				
				bQuit.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						playSound(v.getContext());
						AndroidGunz.onButtonQuit();
					}
				});
				
				bOptions.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						playSound(c);
						((Activity)c).removeDialog(Menus.MENU_OPTIONS);
						((Activity)c).showDialog(Menus.MENU_OPTIONS);
					}
				});
				
				bMusic.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						playSound(c);
						((Activity)c).showDialog(Menus.MENU_MUSIC);
					}
				});
				
				//d.setContentView(layout);
				currentDlg = new Dialog(c, R.style.GameMenu);
				currentDlg.setContentView(layout);
				//d.setTitle("Main");
				/*builder = new AlertDialog.Builder(c);Dialog d = builder.create();
				builder.setView(layout);
				builder.setInverseBackgroundForced(false);
				d = builder.create();*/
				break;
			case(MENU_MUSIC):
				
				View layout5 = inflater.inflate(R.layout.dlg_music,null);
			
				TextView mLabel1 = (TextView)layout5.findViewById(R.id.music_select_none);
				TextView mLabel2 = (TextView)layout5.findViewById(R.id.music_select_007);
				TextView mLabel3 = (TextView)layout5.findViewById(R.id.music_select_halo);
				TextView mLabel4 = (TextView)layout5.findViewById(R.id.music_select_spy1);
				TextView mLabel5 = (TextView)layout5.findViewById(R.id.music_select_spy2);
				
				mLabel1.setTypeface(arFont);
				mLabel2.setTypeface(arFont);
				mLabel3.setTypeface(arFont);
				mLabel4.setTypeface(arFont);
				mLabel5.setTypeface(arFont);
				
				mLabel1.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						AndroidGunz.onOptionChangedMusicTrack(Options.MUSIC_TRACK_NONE);
					}
				});
				mLabel2.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						AndroidGunz.onOptionChangedMusicTrack(Options.MUSIC_TRACK_007);
					}
				});
				mLabel3.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						AndroidGunz.onOptionChangedMusicTrack(Options.MUSIC_TRACK_HALO);
					}
				});
				mLabel4.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						AndroidGunz.onOptionChangedMusicTrack(Options.MUSIC_TRACK_SPY);
					}
				});
				mLabel5.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						AndroidGunz.onOptionChangedMusicTrack(Options.MUSIC_TRACK_SPY2);
					}
				});
				
				
				currentDlg = new Dialog(c, R.style.GameMenu);
				currentDlg.setContentView(layout5);
				
				break;
			case(MENU_OPTIONS):
				View layout4 = inflater.inflate(R.layout.dlg_options,null);
				
				TextView bLabel1 = (TextView)layout4.findViewById(R.id.label_option_johnwoo);
				TextView bLabel2 = (TextView)layout4.findViewById(R.id.label_option_autoreload);
				TextView bLabel3 = (TextView)layout4.findViewById(R.id.label_option_music_repeat);
				TextView bLabel4 = (TextView)layout4.findViewById(R.id.label_option_fd);
				TextView bLabel5 = (TextView)layout4.findViewById(R.id.label_option_fd_hitbox);
				TextView bLabel6 = (TextView)layout4.findViewById(R.id.label_option_headshot);
				
				bLabel1.setTypeface(arFont);
				bLabel2.setTypeface(arFont);
				bLabel3.setTypeface(arFont);
				bLabel4.setTypeface(arFont);
				bLabel5.setTypeface(arFont);
				bLabel6.setTypeface(arFont);
				
				CheckBox cbJohnWoo = (CheckBox)layout4.findViewById(R.id.cb_johnwoo);
				CheckBox cbAutoReload = (CheckBox)layout4.findViewById(R.id.cb_autoreload);
				CheckBox cbMusicRepeat = (CheckBox)layout4.findViewById(R.id.cb_music_repeat);
				
				CheckBox cbFaceDetect = (CheckBox)layout4.findViewById(R.id.cb_fd);
				CheckBox cbHitbox = (CheckBox)layout4.findViewById(R.id.cb_fd_hitbox);
				CheckBox cbHeadshot = (CheckBox)layout4.findViewById(R.id.cb_headshot);
				
				Options o = MenuActivity.getOptions(c);
				cbJohnWoo.setChecked(o.getSwitch(Options.PREF_KEY_OPTION_JOHNWOO,FirearmConfig.DEFAULT_JOHNWOO));
				cbAutoReload.setChecked(o.getSwitch(Options.PREF_KEY_OPTION_AUTORELOAD,FirearmConfig.DEFAULT_AUTORELOAD));
				cbFaceDetect.setChecked(o.getSwitch(Options.PREF_KEY_OPTION_FACEDETECT,FirearmConfig.DEFAULT_FACEDETECT));
				cbHitbox.setChecked(o.getSwitch(Options.PREF_KEY_OPTION_HITBOXES,FirearmConfig.DEFAULT_HITBOXES));
				cbHeadshot.setChecked(o.getSwitch(Options.PREF_KEY_OPTION_HEADSHOTS,FirearmConfig.DEFAULT_HEADSHOTS));
				cbMusicRepeat.setChecked(o.getSwitch(Options.PREF_KEY_OPTION_MUSIC_REPEAT,FirearmConfig.DEFAULT_MUSIC_REPEAT));
				
				cbJohnWoo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						if(AndroidGunz.LITE_MODE){
							AndroidGunz.onOptionNotPurchased();
							buttonView.setChecked(!isChecked);
						}
						else
							((AndroidGunz)c).onOptionChangedJohnWoo(isChecked);
					}
				});
				
				cbAutoReload.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						((AndroidGunz)c).onOptionChangedAutoReload(isChecked);
					}
				});
				cbMusicRepeat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						AndroidGunz.onOptionChangedMusicRepeat(isChecked);
					}
				});
				cbFaceDetect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						if(AndroidGunz.LITE_MODE){
							AndroidGunz.onOptionNotPurchased();
							buttonView.setChecked(!isChecked);
						}
						else
							((AndroidGunz)c).onOptionChangedFacedetect(isChecked);
					}
				});
				cbHitbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						if(AndroidGunz.LITE_MODE){
							AndroidGunz.onOptionNotPurchased();
							buttonView.setChecked(!isChecked);
						}
						else
							((AndroidGunz)c).onOptionChangedHitbox(isChecked);
					}
				});
				cbHeadshot.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						if(AndroidGunz.LITE_MODE){
							AndroidGunz.onOptionNotPurchased();
							buttonView.setChecked(!isChecked);
						}
						else
							((AndroidGunz)c).onOptionChangedHeadshot(isChecked);
					}
				});
				
				currentDlg = new Dialog(c, R.style.GameMenu);
				currentDlg.setContentView(layout4);
				
				break;
			case(MENU_TARGET):
				View layout2 = inflater.inflate(R.layout.dlg_target,null);
				TextView bImport = (TextView)layout2.findViewById(R.id.b_target_import);
				TextView bRefresh = (TextView)layout2.findViewById(R.id.b_target_refresh);
				TextView bBrowse = (TextView)layout2.findViewById(R.id.b_target_browse);
				
				bImport.setTypeface(arFont);
				bRefresh.setTypeface(arFont);
				bBrowse.setTypeface(arFont);
				
				bImport.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						playSound(v.getContext());
						AndroidGunz.onButtonTargetimport();
					}
				});
				bRefresh.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						playSound(v.getContext());
						if(GameController.targetMode==AndroidGunz.TARGET_MODE_DEFAULT){
							((AndroidGunz)c).gameView.refreshTarget();
						}
						AndroidGunz.removeAllDialogs();
					}
				});
				bBrowse.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						playSound(v.getContext());
						currentDlg.dismiss();
						((Activity)c).showDialog(Menus.MENU_TARGETSELECT);
					}
				});
				
				currentDlg = new Dialog(c, R.style.GameMenu);
				currentDlg.setContentView(layout2);
				
				break;
			case(MENU_TARGETSELECT):
				View layout3 = inflater.inflate(R.layout.dlg_targetmenu,null);
				final String[] targetList = c.getResources().getStringArray(R.array.targets_enabled);
			
				final ImageView targetView = (ImageView)layout3.findViewById(R.id.targetselectview);
				TextView bSelect = (TextView)layout3.findViewById(R.id.b_target_select);
				TextView bNext = (TextView)layout3.findViewById(R.id.b_target_next);
				TextView bPrev = (TextView)layout3.findViewById(R.id.b_target_prev);
				
				bSelect.setTypeface(arFont);
				bNext.setTypeface(arFont);
				bPrev.setTypeface(arFont);
				
				String[] tmpTarget = targetList[1].split("\\|");
				targetView.setImageResource(getResourceId(c,tmpTarget[0]));
				targetView.invalidate();
				
				targetView.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						playSound(v.getContext());
						String[] tmpTarget = targetList[browseIdx].split("\\|");
						((AndroidGunz)c).onButtonTargetSet(getResourceId(c,tmpTarget[0]));
						((AndroidGunz)c).gameView.refreshTarget();
					}
				});
				
				bNext.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						
						browseIdx++;
						if(browseIdx>(targetList.length-1))
							browseIdx = 0;
						
						if(DBG_MODE)
							Log.i("Menus","browse index set to: "+browseIdx);
						
						String tmpTarget[] = targetList[browseIdx].split("\\|");
						targetView.setImageResource(getResourceId(c,tmpTarget[0]));
						targetView.invalidate();
						playSound(v.getContext());
					}
				});
				bPrev.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						
						browseIdx--;
						if(browseIdx<0)
							browseIdx = targetList.length-1;
						
						if(DBG_MODE)
							Log.i("Menus","browse index set to: "+browseIdx);
						
						String tmpTarget[] = targetList[browseIdx].split("\\|");
						targetView.setImageResource(getResourceId(c,tmpTarget[0]));
						targetView.invalidate();
						playSound(v.getContext());
					}
				});
				
				
				currentDlg = new Dialog(c, R.style.GameMenu);
				currentDlg.setContentView(layout3);
				
				break;
			case(MENU_EXIT):
				
				View layout8 = inflater.inflate(R.layout.dlg_exit,null);
			
				TextView t1 = (TextView)layout8.findViewById(R.id.b_main);
				Button b2 = (Button)layout8.findViewById(R.id.b_exitnow);
				Button b3 = (Button)layout8.findViewById(R.id.b_moregames);
				
				if(!AndroidGunz.HAS_GOOGLE_MARKET){
					b3.setVisibility(View.GONE);
				}
				
				b2.setTypeface(arFont);
				b3.setTypeface(arFont);
				t1.setTypeface(arFont);
				
				b2.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						((Activity)c).finish();
					}
				});
				b3.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						Intent i = new Intent(Intent.ACTION_VIEW, 
												Uri.parse("market://search?q=GunZ"));
						c.startActivity(i);
						((Activity)c).removeDialog(MENU_EXIT);
					}
				});
				
				currentDlg = new Dialog(c, R.style.GameMenu);
				currentDlg.setContentView(layout8);
				
				break;
			case(MENU_OOM):
				View layout7 = inflater.inflate(R.layout.dlg_oom,null);
			
				Button b1 = (Button)layout7.findViewById(R.id.b_ok);
				
				b1.setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						((Activity)c).finish();
					}
				});
				
				currentDlg = new Dialog(c, R.style.GameMenu);
				currentDlg.setContentView(layout7);
				
			break;
		}
		return currentDlg;
	}
	
	private int getResourceId(Context c, String resName){
		int tmpResId = c.getResources().getIdentifier(resName, "drawable", AndroidGunz.getCtx().getPackageName());
		return tmpResId;
	}
	
}
